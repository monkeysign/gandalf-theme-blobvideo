@extends('inc.layout')

@section('content')
    <div class="row">
        <div class="col">
            <div class="jumbotron">
                <h1 class="display-4">Benvenuto!</h1>
                <p class="lead">Questo è il tema "Demo".</p>
                <hr class="my-4">
                <p>Puoi usarlo come base per la scrittura di un nuovo tema o per impostare la tua applicazione.</p>
                <p class="lead">
                    <a class="btn btn-primary btn-lg" href="{{path_for("page", array('permalink' => 'about'))}}" role="button">Vai alla pagina About</a>
                </p>
            </div>
        </div>
    </div>
@endsection