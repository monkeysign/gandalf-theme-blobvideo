@extends('inc.layout')

@section('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Carrello</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->


    <section class="flat-checkout">
        <div class="container">
            <form name="form-checkout" id="form-checkout" action="{{ path_for('save_order') }}" method="POST">
                <input type="hidden" name="order[payment_status]" value="0">
                <input type="hidden" name="order[status]" value="0">
                <input type="hidden" name="order[id_ship]" value="{{$idShip}}">
                <div class="row">
                    <div class="col-md-7">
                        <div class="box-checkout">
                            <h3 class="title">Checkout</h3>
                            @if(!user_logged())
                                <div class="checkout-login">
                                    Se sei un Utente Registrato Accedi al <a href="{{path_for('loginp',array())}}" title=""> tuo account</a>, altrimenti segui le indicazioni sottostanti per registrarti.
                                </div>
                            @endif
                            <input type="hidden" name="billing_address[id]" value="{{$billing_address->id}}">
                            <input type="hidden" name="billing_address[type]" value="2">
                            <input type="hidden" name="billing_address[selected]" value="1">

                            <div class="billing-fields">
                                <div class="fields-title">
                                    <h3>Indirizzo Primario</h3>
                                    <span></span>
                                    <div class="clearfix"></div>
                                </div><!-- /.fields-title -->
                                <div class="fields-content">
                                    <div class="field-row">
                                        <p class="field-one-half col-form">
                                            <label for="first-name">Nome *</label>
                                            <input type="text" name="billing_address[name]" required placeholder="Nome" value="{{$user->name}}">
                                        </p>
                                        <p class="field-one-half col-form">
                                            <label for="last-name">Cognome *</label>
                                            <input type="text" name="billing_address[surname]" required placeholder="Cognome" value="{{$user->surname}}">
                                        </p>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="field-row">
                                        <p class="field-one-half col-form">
                                            <label for="email-address">Email *</label>
                                            <input type="email" id="email-address" name="billing_address[email]" placeholder="Email" required value="{{$user->email}}">
                                        </p>
                                        <p class="field-one-half col-form">
                                            <label for="phone">Telefono *</label>
                                            <input type="text" id="phone" name="billing_address[phone]" required="true" placeholder="Telefono" value="{{$billing_address->phone}}">
                                        </p>
                                        <div class="clearfix"></div>
                                    </div>
                                @if(!user_logged())
                                        <div class="field-row">
                                            <p class="field-one-half col-form">
                                                <label for="email-address">Password *</label>
                                                <input type="password" name="customer[password]" required placeholder="Password">
                                            </p>
                                            <p class="field-one-half col-form">
                                                <label for="phone">Conferma Password *</label>
                                                <input type="password" name="customer[passwordRepeat]" required placeholder="Ripeti Password">
                                            </p>
                                        </div>
                                    @endif
                                    <div class="field-row col-form">
                                        <label for="phone">Nazione *</label>
                                        <input type="text"  name="billing_address[nation]" placeholder="Nazione" required value="{{$billing_address->nation}}">
                                    </div>
                                    <div class="field-row col-form">
                                        <label for="address">Indirizzo *</label>
                                        <input type="text" id="address" name="billing_address[address]" required placeholder="Indirizzo" value="{{$billing_address->address}}" >
                                    </div>
                                    <div class="field-row col-form">
                                        <label for="town-city ">Citt&agrave; *</label>
                                        <input type="text" id="town-city" name="billing_address[city]" placeholder="Città" required value="{{$billing_address->city}}">
                                    </div>
                                    <div class="field-row ">
                                        <p class="field-one-half col-form">
                                            <label for="state-country">Provincia *</label>
                                            <input type="text" id="state-country" name="billing_address[state]" placeholder="Provincia" required value="{{$billing_address->state}}">
                                        </p>
                                        <p class="field-one-half col-form">
                                            <label for="post-code">Cap *</label>
                                            <input type="text" id="post-code" name="billing_address[zipcode]" placeholder="Cap" required value="{{$billing_address->zipcode}}">
                                        </p>
                                        <div class="clearfix"></div>
                                    </div>
                                    @if(!user_logged())
                                     <!--   <div class="checkbox">
                                            <input type="checkbox" id="create-account" name="create-account" checked value="1">
                                            <label for="create-account">Create an account?</label>
                                        </div>-->
                                    @endif

                                </div><!-- /.fields-content -->
                            </div><!-- /.billing-fields -->
                            <div class="checkbox">
                                <input type="checkbox" id="check-address-ship" name="check-address-ship" value="1">
                                <label for="check-address-ship">Spedire ad un indirizzo differente?</label>
                            </div>

                            <div class="shipping-address-fields visibility-none" id="box-address-ship">
                                <div class="fields-title">
                                    <h3>Indirizzo Secondario</h3>
                                    <span></span>
                                    <div class="clearfix"></div>
                                </div><!-- /.fields-title -->
                                <input type="hidden" name="ship_address[id]" value="{{$ship_address->id}}">
                                <input type="hidden" name="ship_address[type]" value="1">
                                <input type="hidden" name="ship_address[selected]" value="1">

                                <div class="fields-content">
                                    <div class="field-row col-form">
                                        <p class="field-one-half">
                                            <label for="first-name">Nome *</label>
                                            <input type="text" name="ship_address[name]" placeholder="Nome" required value="{{$user->name}}">
                                        </p>
                                        <p class="field-one-half">
                                            <label for="last-name">Cognome *</label>
                                            <input type="text" name="ship_address[surname]" placeholder="Cognome" required value="{{$user->surname}}">
                                        </p>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="field-row col-form">
                                        <p class="field-one-half">
                                            <label for="email-address">Email *</label>
                                            <input type="email" id="email-address" name="ship_address[email]" placeholder="Email" required value="{{$user->email}}">
                                        </p>
                                        <p class="field-one-half">
                                            <label for="phone">Telefono *</label>
                                            <input type="text" id="phone" name="ship_address[phone]" placeholder="Telefono" required value="{{$ship_address->phone}}">
                                        </p>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="field-row col-form">
                                        <label for="phone">Nazione *</label>
                                        <input type="text" id="phone" name="ship_address[nation]" placeholder="Nazione" required value="{{$ship_address->nation}}">
                                    </div>
                                    <div class="field-row col-form">
                                        <label for="address">Indirizzo *</label>
                                        <input type="text" id="address" name="ship_address[address]" placeholder="Indirizzo" required placeholder="Street address" value="{{$ship_address->address}}" >
                                    </div>
                                    <div class="field-row col-form">
                                        <label for="town-city">Citt&agrave; *</label>
                                        <input type="text" id="town-city" name="ship_address[city]" placeholder="Città" required value="{{$ship_address->city}}">
                                    </div>
                                    <div class="field-row col-form">
                                        <p class="field-one-half">
                                            <label for="state-country">Provincia *</label>
                                            <input type="text" id="state-country" name="ship_address[state]" placeholder="Provicia" required value="{{$ship_address->state}}">
                                        </p>
                                        <p class="field-one-half col-form">
                                            <label for="post-code">Cap *</label>
                                            <input type="text" id="post-code" name="ship_address[zipcode]" placeholder="Cap" required value="{{$ship_address->zipcode}}">
                                        </p>
                                        <div class="clearfix"></div>
                                    </div>
                                </div><!-- /.fields-content -->
                            </div><!-- /.shipping-address-fields -->
                            <div class="field-row">
                                <label for="notes">Note Ordine</label>
                                <textarea placeholder="note ordine" name="order[note]"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="small">I campi contrassegnati con * sono da considerarsi obbligatori</p>
                                </div>
                            </div>
                        </div><!-- /.box-checkout -->
                    </div><!-- /.col-md-7 -->
                    <div class="col-md-5">
                        <div class="cart-totals style2">
                            <h3>Il Tuo Ordine</h3>
                            <table class="product">
                                <thead>
                                <tr>
                                    <th>Prodotto</th>
                                    <th>Quantit&agrave;</th>
                                    <th>Totale</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($product)
                                    @foreach($product as $prodotto)
                                        <tr>
                                            <td>
                                                {{$prodotto->title}}
                                            </td>
                                            <td>
                                                {{$prodotto->quantity}}
                                            </td>
                                            <td class="text-right">
                                                {{ number_format($prodotto->price_cart * $prodotto->quantity, 2, ',', '.') }}&euro;
                                            </td>
                                            <td>
                                                <a href="#" title="">
                                                    <img src="images/icons/delete.png" alt="">
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table><!-- /.product -->
                            <table>
                                <tbody>
                                <tr>
                                    <td>Totale</td>
                                    <td class="subtotal">{{$sub_total}}  &euro;</td>
                                </tr>
                                <tr>
                                    <td>{{$spedizione->label_title}}</td>
                                    <td class="subtotal">{{$spedizione->label_cart}}</td>
                                </tr>
                                <tr id="row-cost-standand">
                                    <td>Totale Ordine</td>
                                    <td class="price-total">{{$total}}  &euro;</td>
                                </tr>
                                <tr class="row-cost-paypal visibility-none" >
                                    <td>Costo gestione Paypal*</td>
                                    <td class="subtotal">{{$cost_paypal}}  &euro;</td>
                                </tr>
                                <tr class="row-cost-paypal hidden  visibility-none">
                                    <td>Totale</td>
                                    <td class="price-total">{{$total_paypal}}  &euro;</td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="btn-radio style2">
                                <label for="flat-payment">Metodo di pagamento</label>
                                <p>Seleziona il tuo metodo di pagamento preferito</p>
                                @if($method_payment)
                                    @foreach($method_payment as $payment)
                                        <div class="radio-info">
                                            <input type="radio" class="radio-metodo-pagamento" id="{{$payment->code}}" name="order[payment_type]" @if($payment->code=='bonifico') checked @endif value="{{$payment->id}}">
                                            <label for="{{$payment->code}}">{{$payment->title}}</label>
                                        </div>
                                    @endforeach
                                @endif

                            </div><!-- /.btn-radio style2 -->
                            <div class="checkbox col-form">
                                <input type="checkbox" id="checked-order" name="checked-order" required>
                                <label for="checked-order"><a href="{{ path_for('single-page', ['permalink' => 'privacy']) }}">Ho letto ed accetto Privacy e Cookie Policy*</a></label>
                                <span id="result"> </span>
                            </div><!-- /.checkbox -->
                            <div class="btn-order">
                                <a href="#" class="order" id="submit-save-order" title="">Procedi al Pagamento ></a>
                            </div><!-- /.btn-order -->

                        </div><!-- /.cart-totals style2 -->
                    </div><!-- /.col-md-5 -->
                </div><!-- /.row -->
            </form>
        </div><!-- /.container -->

    </section><!-- /.flat-checkout -->

    <section class="flat-row flat-iconbox style3">
        <div class="container">
            <div class="row">
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/car.png" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Spedizioni Gratis</h4>
                                <p>per ordini superiori a 79&euro;</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/car.png" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Consegne 24/48 H</h4>
                                <p>con corriere espresso</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/payment.png" alt="">
                            </div>
                            <div class="box-title">
                                <h3>Pagamenti Sicuri</h3>
                                <p>con PayPal e Bonifico</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/return.png" alt="">
                            </div>
                            <div class="box-title">
                                <h3>Registrati al Sito</h3>
                                <p>per Offerte e Promozioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/order.png" alt="">
                            </div>
                            <div class="box-title">
                                <h3>Contattaci</h3>
                                <p>per maggiori informazioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-iconbox -->


    <div style="display: none">
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="business" value="paypal@blobvideo.com">
            <input type="hidden" name="return" value="{{ path_for('single-page', ['permalink' => 'success-checkout-paypal']) }}" />
            <input type="hidden" name="cancel_return" value="{{ path_for('single-page', ['permalink' => 'error-checkout']) }}" />
            <input type="hidden" name="notify_url" value="{{ path_for('pagamento-paypal') }}" />
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="currency_code" value="EUR">
            <input type="hidden" id="titoloPaypal" name="item_name" value="Ordine ">
            <input type="hidden" id="costoPaypal" name="amount"  value="{{$total_paypal_form}}">
            <input type="hidden" name="custom" id="ordineid">
            <input style="width: 120px" type="image" src="" border="0" id="submitPaypal" name="submit" alt="PayPal - Il metodo rapido, affidabile e innovativo per pagare e farsi pagare.">
        </form>
    </div>



@endsection
@section('scripts')

    <script>
        $(function () {



            $( "#submit-save-order" ).click(function() {
                $( "#form-checkout" ).submit();
            });

            $(".radio-metodo-pagamento").change(function() {
                if ($(this).val() == '2') { // contrassegno
                    $('.row-cost-paypal').removeClass('visibility-none');
                    $('#row-cost-standand').addClass('visibility-none');
                } else {
                    $('.row-cost-paypal').addClass('visibility-none');
                    $('#row-cost-standand').removeClass('visibility-none');
                }
            });



            $('#check-address-ship').change(function() {

                if(this.checked != true){
                    $(".shipping-address-fields").hide();
                }else{
                    $(".shipping-address-fields").show();
                }
            });

            $("#form-checkout").validate({
                submitHandler: function(form) {
                    $('.response').removeClass('btn-success btn-danger').addClass('btn btn-default').html("Operazione in corso");
                    $('#' + form.id).ajaxSubmit({
                        success: showResponse,
                        dataType: "json"
                    });
                    return false;
                },
                errorClass: "help-block",
                errorElement: "div",
                rules: {
                    "billing_address[name]": {required: !0},
                    "billing_address[surname]": {required: !0},
                    "billing_address[email]": {required: !0},
                    "billing_address[phone]": {required: !0},
                    "billing_address[nation]": {required: !0},
                    "billing_address[address]": {required: !0},
                    "billing_address[city]": {required: !0},
                    "billing_address[state]": {required: !0},
                    "billing_address[zipcode]": {required: !0},
                    "ship_address[name]": {required: !0},
                    "ship_address[surname]": {required: !0},
                    "ship_address[email]": {required: !0},
                    "ship_address[phone]": {required: !0},
                    "ship_address[nation]": {required: !0},
                    "ship_address[address]": {required: !0},
                    "ship_address[city]": {required: !0},
                    "ship_address[state]": {required: !0},
                    "ship_address[zipcode]": {required: !0},
                    "checked-order": {required: !0},
                    "customer[password]": {required: !0},
                    "customer[passwordRepeat]": {required: !0}
                },
                messages: {
                    "billing_address[name]": "Questo campo è obbligatorio.",
                    "billing_address[surname]": "Questo campo è obbligatorio.",
                    "billing_address[email]": "Questo campo è obbligatorio.",
                    "billing_address[phone]": "Questo campo è obbligatorio.",
                    "billing_address[nation]": "Questo campo è obbligatorio.",
                    "billing_address[address]": "Questo campo è obbligatorio.",
                    "billing_address[city]": "Questo campo è obbligatorio.",
                    "billing_address[state]": "Questo campo è obbligatorio.",
                    "billing_address[zipcode]": "Questo campo è obbligatorio.",
                    "ship_address[name]": "Questo campo è obbligatorio.",
                    "ship_address[surname]": "Questo campo è obbligatorio.",
                    "ship_address[email]": "Questo campo è obbligatorio.",
                    "ship_address[phone]": "Questo campo è obbligatorio.",
                    "ship_address[nation]": "Questo campo è obbligatorio.",
                    "ship_address[address]": "Questo campo è obbligatorio.",
                    "ship_address[city]": "Questo campo è obbligatorio.",
                    "ship_address[state]": "Questo campo è obbligatorio.",
                    "ship_address[zipcode]": "Questo campo è obbligatorio.",
                    "checked-order": "Questo campo è obbligatorio.",
                    "customer[password]": "Questo campo è obbligatorio.",
                    "customer[passwordRepeat]": "Questo campo è obbligatorio."
                },
                errorPlacement: function(e, t) {
                    t.parents(".col-form").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".col-form").removeClass("has-success has-error").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".col-form").removeClass("has-success has-error").addClass("has-success"), e.closest(".help-block").remove();
                }
            });

        });

        function showResponse(responseText, statusText, xhr, $form) {
            var res = responseText.result;
            var order;
            if (!isNaN(res)) {
                order = responseText.order;
                console.log(order.payment_type);

                if (order.payment_type == 2) {

                    //$('.response').addClass('btn btn-default').html("<i class='glyphicon-cogwheels'></i> Attendi il reindirizzamento alla pagina di paypal");
                    $('#titoloPaypal').val("Ordine n " + res);
                    $('#ordineid').val(res);
                    $("#submitPaypal").trigger('click');

                } else {
                    $(window.location).attr('href', '{{ path_for('single-page', ['permalink' => 'success-checkout']) }}');
                }
            } else {
                $("#result").addClass("btn btn-danger").html(res);
            }
        }

    </script>
@endsection