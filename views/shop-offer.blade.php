@extends('inc.layout')

@section('content')
    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <?php
                        $i = 1;
                        $len = count($breadcrumb);
                        ?>
                        @foreach($breadcrumb as $key => $voice)
                            <li class="<?= ($i < $len) ? 'trail-item' : 'trail-end' ?> ">
                                <a href="{{ path_for('shop-page-cat', ['category' => $key]) }}" title="">{{$voice}}</a>
                                @if($i < $len)<span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>@endif
                                <?php $i++; ?>
                            </li>
                        @endforeach
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->

    <main id="shop">
        <form id="listShopForm" type="GET" action="">
            <input type="hidden" name="term" value="{{$term}}"/>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="sidebar w-100">

                            <div class="widget widget-brands mt-5">
                                <div class="widget-title">
                                    <h3>Filtra per:<span class="active d-block d-md-none"></span></h3>
                                </div>
                                <div class="widget-content mobile-toggle">

                                    <div class="widget widget-brands">
                                        <div class="widget-title">
                                            <h3>Tipologia Prodotto<span class="active"></span></h3>
                                        </div>
                                        <div class="widget-content" style="display:none;">
                                            <ul class="box-checkbox scroll">
                                                @foreach($allProductType as $productType)
                                                    <li class="check-box">
                                                        <input class="brand-s"
                                                               <?= (in_array($productType->id, array_keys($productTypeSelected))) ? 'checked' : '' ?> type="checkbox"
                                                               id="product_type{{$productType->id}}"
                                                               name="product_type[{{$productType->id}}]">
                                                        <label for="product_type{{$productType->id}}">{{$productType->title}}</label>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div><!-- /.widget widget-brands -->
                                </div>
                            </div>
                        </div><!-- /.sidebar -->
                    </div><!-- /.col-lg-3 col-md-4 -->
                    <div class="col-lg-9 col-md-8">
                        <div class="main-shop">

                            <div class="mb-2">
                                <img src="{{the_media($bannerHome->meta('banner_offerte'))}}"
                                     class="mw-100"/>
                            </div>

                            <div class="wrap-imagebox">
                                <div class="flat-row-title">
                                    <h3>Offerte</h3>
                                    <span>
										Trovati {{number_format($query->get('count'),0,'.','.')}} prodotti
									</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="sort-product">
                                    <ul class="icons">
                                        <li>
                                            <img src="{{asset('assets/images/')}}icons/list-1.png" alt="">
                                        </li>
                                        <li>
                                            <img src="{{asset('assets/images/')}}icons/list-2.png" alt="">
                                        </li>
                                    </ul>
                                    <div class="sort">
                                        <div class="popularity">
                                            <select name="ord">
                                                <option value="id|desc" {{($query->get('ord') == 'id|desc') ? 'selected' : ''}}>
                                                    Rilevanza
                                                </option>
                                                <option value="title|asc" {{($query->get('ord') == 'title|asc') ? 'selected' : ''}}>
                                                    A-Z
                                                </option>
                                                <option value="title|desc" {{($query->get('ord') == 'title|desc') ? 'selected' : ''}}>
                                                    Z-A
                                                </option>
                                                <option value="price|asc" {{($query->get('ord') == 'price|asc') ? 'selected'  : ''}}>
                                                    Prezzo Crescente
                                                </option>
                                                <option value="price|desc" {{($query->get('ord') == 'price|desc') ? 'selected'  : ''}}>
                                                    Prezzo Decrescente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="showed">
                                            <select name="l">
                                                <option value="15" {{($query->get('l') == 15) ? 'selected' : ''}}>Mostra
                                                    15
                                                </option>
                                                <option value="30" {{($query->get('l') == 30) ? 'selected' : ''}}>Mostra
                                                    30
                                                </option>
                                                <option value="60" {{($query->get('l') == 60) ? 'selected' : ''}}>Mostra
                                                    60
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="tab-product">
                                    <div class="row sort-box">
                                        @foreach($products as $product)
                                            <div class="col-lg-4 col-sm-6">
                                                <div class="product-box">
                                                    <div class="imagebox">
                                                        <div class="status-product">
                                                            @if ($product->state_product == 1)
                                                                <div class="usato">
                                                                    Usato @if ($product->meta('usato_garantito') == 1)
                                                                        Garantito @endif</div>
                                                            @elseif($product->state_product == 2)
                                                                <div class="inarrivo">
                                                                    In Arrivo
                                                                </div>
                                                            @elseif($product->state_product == 3)
                                                                <div class="inrientro">
                                                                    In Rientro
                                                                </div>
                                                            @elseif ($product->offer == 1)
                                                                <div class="offerlabel">Offerta</div>
                                                            @endif
                                                        </div>
                                                        <div class="box-image">
                                                            <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                               title="">
                                                                <img src="{{config('httpmedia').'ecommerce/prodotti/'.$product->meta('imghighlight')}}"
                                                                     alt="">
                                                            </a>
                                                        </div><!-- /.box-image -->
                                                        <div class="box-content">
                                                            <div class="cat-name">
                                                                <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                                   title="">{{(isset($product->category) && $product->category->title) ? $product->category->title : "Nessuna Categoria" }}</a>
                                                            </div>
                                                            <div class="product-name">
                                                                <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                                   title="">{{$product->title}}</a>
                                                            </div>
                                                            @if($product->state_product != 3)
                                                                <div class="price">
                                                                    <span class="sale">&euro;{{($product->price_offer > 0 ) ? $product->get_price_offer() : $product->get_price()}}</span>
                                                                    @if($product->price_offer > 0) <span
                                                                            class="regular">{{$product->get_price()}}</span> @endif
                                                                </div>
                                                            @endif
                                                        </div><!-- /.box-content -->
                                                        <div class="box-bottom">
                                                            <div class="btn-add-cart">
                                                                <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                                   title="">
                                                                    <img src="{{asset('assets/images/')}}icons/add-cart.png"
                                                                         alt="">Vai al Prodotto
                                                                </a>
                                                            </div>
                                                            @if(user_logged())
                                                                <div class="compare-wishlist">
                                                                    <a href="#" class="wishlist link-wishlist wish_{{$product->id}}" title="" data-idwish="{{$product->id}}">
                                                                        <i class="fa fa-heart"> </i> <span>Lista dei desideri</span>
                                                                    </a>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        {{--<div class="box-bottom">--}}
                                                        {{--<div class="btn-add-cart">--}}
                                                        {{--<a href="#" title="">--}}
                                                        {{--<img src="{{asset('assets/images/')}}icons/add-cart.png"--}}
                                                        {{--alt="">Aggiungi al--}}
                                                        {{--carrello {{$product->meta('imghighlight')}}--}}
                                                        {{--</a>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="compare-wishlist">--}}
                                                        {{--<a href="#" class="compare" title="">--}}
                                                        {{--<img src="{{asset('assets/images/')}}icons/compare.png"--}}
                                                        {{--alt="">Compare--}}
                                                        {{--</a>--}}
                                                        {{--<a href="#" class="wishlist" title="">--}}
                                                        {{--<img src="{{asset('assets/images/')}}icons/wishlist.png"--}}
                                                        {{--alt="">Lista dei desideri--}}
                                                        {{--</a>--}}
                                                        {{--</div>--}}
                                                        {{--</div><!-- /.box-bottom -->--}}
                                                    </div><!-- /.imagebox -->
                                                </div>
                                            </div><!-- /.col-lg-4 col-sm-6 -->
                                        @endforeach


                                    </div><!-- /.col-lg-4 col-sm-6 -->
                                    <div class="sort-box">
                                        @foreach($products as $product)
                                            <div class="product-box style3" style="height: 270px!important">
                                                <div class="imagebox style1 v3" style="position:relative">
                                                    <div class="status-product">
                                                        @if ($product->state_product == 1)
                                                            <div class="usato">
                                                                Usato @if ($product->meta('usato_garantito') == 1)
                                                                    Garantito @endif</div>
                                                        @endif

                                                        @if ($product->offer == 1)
                                                            <div class="offerlabel">Offerta</div>
                                                        @endif
                                                    </div>

                                                    <div class="grade-product small">
                                                        @if ($product->grade == 'A' || $product->grade == 'B' || $product->grade == 'AB')
                                                            <div class="gradeimg">
                                                                <img src="{{asset('assets/images/')}}icons/grado-{{strtolower($product->grade)}}.png">
                                                            </div>
                                                        @endif
                                                    </div>

                                                    <div class="mac-powerup-product small">
                                                        @if ($product->mac_powerup == 1)
                                                            <div class="gradeimg">
                                                                <img src="{{asset('assets/images/')}}icons/mac-potenziato.png">
                                                            </div>
                                                        @endif
                                                    </div>

                                                    <div class="box-image">
                                                        <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                           title="">
                                                            <img style="height:200px!important; width:auto!important;"
                                                                 src="{{config('httpmedia').'ecommerce/prodotti/'.$product->meta('imghighlight')}}"
                                                                 alt="">
                                                        </a>
                                                    </div><!-- /.box-image -->
                                                    <div class="box-content">
                                                        <div class="cat-name">
                                                            <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                               title="">{{(isset($product->category) && $product->category->title) ? $product->category->title : "Nessuna Categoria" }}</a>
                                                        </div>
                                                        <div class="product-name">
                                                            <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                               title="">{{$product->title}}</a>
                                                        </div>
                                                        <div class="status">
                                                            @if($product->state_product == 0) Stato: Nuovo <br><span
                                                                    style="color:#6CBE42">Disponibilità: Sì</span>@endif
                                                            @if($product->state_product == 1) Stato: Usato <br><span
                                                                    style="color:#6CBE42">Disponibilità: Sì</span>@endif
                                                            @if($product->state_product == 2) Stato: Nuovo <br><span
                                                                    style="color:#F26522">Disponibilità: In Arrivo</span>@endif
                                                            @if($product->state_product == 3) Stato: Nuovo <br><span
                                                                    style="color:#F26522">Disponibilità: In Rientro</span>@endif
                                                        </div>
                                                        <div class="info">
                                                            {{str_limit(strip_tags($product->description), $limit = 150, $end = '...')}}
                                                        </div>
                                                    </div><!-- /.box-content -->
                                                    <div class="box-price">
                                                        @if($product->state_product != 3)
                                                            <div class="price">
                                                                <span class="sale">&euro;{{($product->price_offer > 0) ? $product->get_price_offer() : $product->get_price()}}</span>
                                                                @if($product->price_offer > 0) <span
                                                                        class="regular">{{$product->get_price()}}</span> @endif
                                                            </div>
                                                        @endif
                                                        {{--<form name="form-carrello" id="form-carrello" action="{{ path_for('save-carrello') }}" method="POST">--}}
                                                        {{--<input type="hidden" name="id_product" value="{{$product->id}}" />--}}
                                                        {{--<input type="hidden" name="quantity" value="1" min="1" max="100" required placeholder="Quantità">--}}

                                                        <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}" title="">
                                                            <button type="button" class="mt-3" style="background: #1F4296; color:#fff; border-radius: 0;">
                                                                {{--<img src="{{asset('assets/images/')}}icons/add-cart.png"--}}
                                                                {{--alt="">--}}
                                                                Vai al Prodotto
                                                            </button>
                                                        </a>
                                                            @if(user_logged())
                                                                <div class="compare-wishlist mt-3">
                                                                    <a href="#" class="wishlist link-wishlist wish_{{$product->id}}" title="" data-idwish="{{$product->id}}">
                                                                        <i class="fa fa-heart"> </i> <span>Lista dei desideri</span>
                                                                    </a>
                                                                </div>
                                                            @endif
                                                        {{--</form>--}}
                                                        {{--<div class="compare-wishlist">--}}
                                                        {{--<a href="#" class="compare" title="">--}}
                                                        {{--<img src="{{asset('assets/images/')}}icons/compare.png"--}}
                                                        {{--alt="">Compare--}}
                                                        {{--</a>--}}
                                                        {{--<a href="#" class="wishlist" title="">--}}
                                                        {{--<img src="{{asset('assets/images/')}}icons/wishlist.png"--}}
                                                        {{--alt="">Wishlist--}}
                                                        {{--</a>--}}
                                                        {{--</div>--}}
                                                    </div><!-- /.box-price -->
                                                </div><!-- /.imagebox -->
                                            </div><!-- /.product-box -->
                                        @endforeach


                                        <div style="height: 9px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.wrap-imagebox -->
                        <div class="blog-pagination text-right">
                        <!--	<span>
									Pagina {{$query->get('p')}} di {{$query->get('totPage')}}
                                </span>

                            <ul class="flat-pagination style1">
                                <li class="active">
                                    <input style="width: 80px; display: inline-block; margin:0; padding:15px; height: 16px;"
                                           type="number" name="p" value="{{$query->get('p')}}">
                                    / {{$query->get('totPage')}}
                                </li>
                            </ul> -->
                            <input style="width: 80px; display: inline-block; margin:0; padding:15px; height: 16px;"
                                   id="page" type="hidden" name="p" value="{{$query->get('p')}}">
                            <ul class="pagination">
                                <!-- AVANTI -->
                                @if(($query->get('p')-1) < 1)
                                    <li class="disabled"><a href="#" class="disabled">&laquo;</a></li>
                                @else
                                    <li><a href="#" data-page="{{$query->get('p') - 1}}">&laquo;</a></li>
                                @endif
                                @if(($query->get('p')-2) > 0 )
                                    <li><a href="#" data-page="1">1</a></li>
                                    <li><a href="#" onclick="return false">...</a></li>
                                @endif
                            <!-- Link Pagine -->
                                <?php $passaggio = 1; ?>
                                @for($i=$query->get('p')-1; ($i<=$query->get('totPage') and $passaggio<4); $i++)
                                    @if($i>0)
                                        <li @if($i==$query->get('p')) class="active" @endif><a href="#"
                                                                                               data-page="{{$i}}">{{$i}}</a>
                                        </li>
                                        <?php $passaggio++; ?>
                                    @endif
                                @endfor

                                @if(($query->get('p')+2) <= $query->get('totPage') )
                                    <li><a href="#" onclick="return false">...</a></li>
                                    <li><a href="#"
                                           data-page="{{$query->get('totPage')}}">{{$query->get('totPage')}}</a></li>
                                @endif
                            <!-- AVANTI -->
                                @if(($query->get('p') +1) >= $query->get('totPage'))
                                    <li class="disabled"><a href="#">&raquo;</a></li>
                                @else
                                    <li><a href="#" data-page="{{$query->get('p') + 1}}">&raquo;</a></li>
                                @endif

                            </ul>
                            <div class="clearfix"></div>
                        </div><!-- /.blog-pagination -->
                    </div><!-- /.main-shop -->
                </div><!-- /.col-lg-9 col-md-8 -->
            </div><!-- /.row -->
        </form>
    </main><!-- /#shop -->
@endsection

@section('scripts')
    <script>
        $('#listShopForm select, #listShopForm input[name="p"], #listShopForm input[name="filp"]').on('change', function () {
            $('#listShopForm').submit();
        })

        $("a[data-page]").on("click", function (e) {
            e.preventDefault();
            var numPag = $(this).data('page');
            $("#page").val(numPag);
            $('#listShopForm').submit();
            return false;

        })

        $('.brand-s').on('click', function () {
            if (this.checked) {
                $(this).prop("disabled", false);
            } else {
                $(this).prop("disabled", true);
            }
            $('#listShopForm').submit();
        })

    </script>
@endsection