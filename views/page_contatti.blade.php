@extends('inc.layout')

@section('content')
    <section class="flat-map">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="flat-map" class="pdmap">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.641105554766!2d14.61037641490559!3d40.747922043355!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x133bb8d5248e581f%3A0xbb670fdd80d51c8e!2sVia+Alcide+de+Gasperi%2C+215%2C+84016+Pagani+SA!5e0!3m2!1sit!2sit!4v1541934146590" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div><!-- /#flat-map -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /#flat-map -->

    <section class="flat-contact style2">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    {!! the_content() !!}
                </div><!-- /.col-md-5 -->
                <div class="col-md-7">
                    <div class="form-contact left">
                        <div class="form-contact-header">
                            <h3>Compila il modulo per contattarci</h3>
                            <hr/>
                        </div><!-- /.form-contact-header -->
                        <div class="form-contact-content">
                            <form action="{{path_for('sendcontact')}}" method="post" id="form-contact" accept-charset="utf-8">
                                <div class="form-box one-half name-contact col-form">
                                    <label for="name-contact">Nominativo*</label>
                                    <input type="text" name="name_contact" placeholder="Nominativo" required>
                                </div>
                                <div class="form-box one-half password-contact col-form">
                                    <label for="password-contact">Email*</label>
                                    <input type="email" name="email_contact" placeholder="Email" required>
                                </div>
                                <div class="form-box col-form">
                                    <label for="comment-contact">Messaggio*</label>
                                    <textarea id="message_contact" name="message_contact" required></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-left col-form">
                                        <input type="checkbox"  required name="informativa" style="opacity: 1"/>Ho letto ed accetto <a class="active" href="{{ path_for('single-page', ['permalink' => 'privacy']) }}">Privacy e Cookie Policy*</a>
                                    </div>
                                </div>
                                <p class="small">I campi contrassegnati con * sono da considerarsi obbligatori</p>
                                <div class="form-box text-center">
                                    <div class="g-recaptcha" data-sitekey="6LenNKQUAAAAAEA9ddKOEQ8P6DGDUHBUPTPMdM6i"></div>
                                    <button type="submit" id="bottone-submit" class="contact">Invia Messaggio ></button>
                                    <span class="response-contact_email"></span>
                                </div>
                            </form><!-- /#form-contact -->

                        </div><!-- /.form-contact-content -->
                    </div><!-- /.form-contact left -->
                </div><!-- /.col-md-7 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-contact style2 -->
@endsection

@section('scripts')
    <script>
        $(function () {
            $("#form-contact").validate({
                submitHandler: function(form) {
                    $('.response-contact_email').removeClass('btn-success btn-danger').addClass('btn btn-primary').html("Operazione in corso");
                    $('#' + form.id).ajaxSubmit({
                        success: showResponseMailContact,
                        dataType: "json"
                    });
                    return false;
                },
                errorClass: "help-block",
                errorElement: "div",
                rules: {
                    name_contact: {required: !0},
                    subscribe_email: {required: !0, email: !0},
                    informativa: {required: !0},
                    message_contact: {required: !0}
                },
                messages: {
                    message_contact: "Campo obbligatorio",
                    email_contact: "Campo obbligatorio",
                    informativa: "Campo obbligatorio",
                    name_contact: "Campo obbligatorio"
                },
                errorPlacement: function(e, t) {
                    t.parents(".col-form").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".col-form").removeClass("has-success has-error ").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".col-form").removeClass("has-success has-error mt-5").addClass("has-success"), e.closest(".help-block").remove();
                }
            });

        });

        function showResponseMailContact(responseText, statusText, xhr, $form) {
            var res = responseText.result;
            if (res==1) {
                $('#bottone-submit').remove();
                $('.response-contact_email').removeClass('btn-success btn-danger').addClass('btn btn-success').html("<p>Email inviata correttamente</p>");
            } else {
                $('.response-contact_email').removeClass('btn btn-success mt-5').addClass('btn btn-danger mt-5').html("<p>Errore nell'invio della mail</p>");
            }
        }

    </script>
@endsection