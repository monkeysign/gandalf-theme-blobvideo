@extends('inc.layout')

@section('content')
    <section style="background:#FFFFFF; padding:100px 0px 100px 0px;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 style="font-size:8em;">Pagina non trovata</h3>
                </div>
            </div>
        </div>
    </section>
    <section style="background:#F5F5F5; padding:100px 0px 100px 0px;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <p style="font-size:1.3em; color:#2A2929;">Siamo spiacenti, ma non riusciamo a trovare quello che stai cercando.</p>
                    <br>
                    <p style="font-size:1.3em; color:#2A2929;">La pagina potrebbe essere stata spostata oppure eliminata.</p>
                    <br>
                    <p style="font-size:1.3em; color:#2A2929;">Se conosci il nome del prodotto che ti interessa, digitalo nel motore di ricerca in alto.</p>
                </div>
            </div>
        </div>
    </section>
@endsection