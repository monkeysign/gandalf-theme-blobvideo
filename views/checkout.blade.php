@extends('inc.layout')

@section('content')
    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Carrello</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->

    <section class="flat-shop-cart">
        <div class="container">
            @if($product)
                <div class="row">
                    <div class="col-lg-8">
                        <div class="flat-row-title style1">
                            <h3>Carrello</h3>
                        </div>
                        <div class="table-cart">
                            <table>
                                <thead>
                                <tr>
                                    <th>Prodotto</th>
                                    <th>Quantit&agrave;</th>
                                    <th>Totale</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($product as $prodotto)
                                    <tr id="pr{{$prodotto->id}}">
                                        <td>
                                            <div class="img-product">
                                                @if($prodotto->meta('imghighlight'))

                                                    <img src="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($prodotto->meta('imghighlight')) }}"
                                                         alt='' width='400' height='300'/>
                                                @endif
                                            </div>
                                            <div class="name-product">
                                                {{$prodotto->title}}
                                            </div>
                                           <!-- <div class="price">
                                                {{$prodotto->price}} &euro;
                                            </div> -->
                                            <div class="clearfix"></div>
                                        </td>
                                        <td>
                                            <div class="quantity">
                                                {{--<span class="btn-down"></span>--}}
                                                {{--<input type="number" readonly name="quantity"--}}
                                                {{--value="{{$prodotto->quantity}}" min="1" max="100"--}}
                                                {{--placeholder="Quantità">--}}
                                                {{--<span class="btn-up"></span>--}}
                                                {{$prodotto->quantity}}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="total">
                                                {{ number_format($prodotto->price_cart * $prodotto->quantity, 2, ',', '.') }}  &euro;
                                            </div>
                                        </td>
                                        <td>
                                            <a href="#" class="remove-cart-row" data-id="{{$prodotto->id}}">
                                                <img src="{{asset('assets/images/')}}icons/delete.png" alt="">
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div><!-- /.table-cart -->
                    </div><!-- /.col-lg-8 -->
                    <div class="col-lg-4">
                        <div class="cart-totals">
                            <h3>Ordine</h3>
                            <form name="form-checkout" id="form-checkout" action="{{ path_for('proceed_checkout') }}"
                                  method="POST">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>Sub Totale</td>
                                        <td class="subtotal">{{$sub_total}} &euro;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-left">Spese di Spedizione
                                            <p id="label_cart_residuo" class="italic small">{!! $spedizione->label_cart_residuo !!} </p>
                                        </td>

                                    </tr>
                                    {{--<tr>--}}
                                        {{--<td ><input type="radio" name="spedizione" value="2"> Ritiro in Sede</td>--}}
                                        {{--<td class="subtotal">--}}
                                          {{--&euro; 0,00--}}
                                        {{--</td>--}}
                                    {{--</tr>--}}
                                    <tr>
                                        <td><input type="radio" name="spedizione" checked value="{{$spedizione->id}}"> {{$spedizione->title}}</td>
                                        <td class="subtotal">
                                          {{$spedizione->label_cart}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Totale</td>
                                        <td class="price-total"> <span id="priceTotal">{{$total}}</span>  &euro;</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="btn-cart-totals">
                                    <a href="#" class="update emptyCart" title="">Svuota Carrello</a>
                                    <a href="#" class="checkout" id="proceed_checkout" title="">Procedi al Checkout</a>
                                </div><!-- /.btn-cart-totals -->
                            </form><!-- /form -->
                        </div><!-- /.cart-totals -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            @else
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <p>Non sono presenti prodotti nel carello</p>
                    </div>
                </div>
            @endif
        </div><!-- /.container -->
    </section><!-- /.flat-shop-cart -->

    <section class="flat-row flat-iconbox style3">
        <div class="container">
            <div class="row">
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/banconota.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Spedizioni Gratis</h4>
                                <p>per ordini superiori a 79&euro;</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/consegna.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Consegne 24/48 H</h4>
                                <p>con corriere espresso</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img style="width:45px;" src="{{asset('assets/images/')}}icons/pagamentisicuri.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Pagamenti Sicuri</h4>
                                <p>con PayPal e Bonifico</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/utente.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Registrati al Sito</h4>
                                <p>per Offerte e Promozioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/fumetto.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Contattaci</h4>
                                <p>per maggiori informazioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-iconbox -->
@endsection

@section('scripts')
    <script>
      $(function () {
        $("#proceed_checkout").click(function () {
          $("#form-checkout").submit();
        });

        $('.remove-cart-row').on('click', function () {
          if (confirm('Sei sicuro di voler rimuovere questa riga?')) {
            var $id = $(this).data('id');
            var cart = JSON.parse(getCookie('checkout'));
            var newCart = cart.filter(function (x) {
              if (parseInt(x.id_product) !== $id) {
                return x;
              }
            });
            newCart = JSON.stringify(newCart);
            setCookie('checkout', newCart);
            location.reload(true);
          }
        })

        $('.emptyCart').on('click', function () {
          if (confirm('Sei sicuro di voler svuotare il carrello?')) {
            var newCart = JSON.stringify([]);
            setCookie('checkout', newCart);
            location.reload(true);
          }
        })



          $('input[name=spedizione]').change(function(){
              var optionSped = $( 'input[name=spedizione]:checked' ).val();
              var myvar = {{$spedizione->id }};
              if(optionSped== myvar){
                  $('#label_cart_residuo').show();
                  $('#priceTotal').html("{{$total }}");

              }

              else{
                  $('#label_cart_residuo').hide();
                  $('#priceTotal').html("{{$sub_total }}");
              }
          });



      });

      function numberWithCommas(x) {
          return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
    </script>
@endsection