@extends('inc.layout')

@section('content')
    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="images/icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-item">
                            <a href="#" title="">Shop</a>
                            <span><img src="images/icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Smartphones</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->

    <section class="main-blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                        @include('inc.sidebar')
                </div><!-- /.col-lg-3 col-md-4 -->
                <div class="col-lg-9 col-md-8">
                    <div class="post-wrap">

                        @if(count($posts) == 0)<h6 class="text-secondary">Archivio vuoto...</h6>@endif
                        @foreach($posts as $single)
                                <article class="main-post">
                                    <div class="featured-post">
                                        <a href="{{ path_for('page', ['permalink' => $single->permalink]) }}" title="">
                                            <img src="images/blog/post-01.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="divider25"></div>
                                    <div class="content-post">
                                        <h3 class="title-post">
                                            <a href="{{ path_for('page', ['permalink' => $single->permalink]) }}" title="">{{$single->meta('title')}}</a>
                                        </h3>
                                        <ul class="meta-post">
                                            <li class="date">
                                                <a href="{{ path_for('page', ['permalink' => $single->permalink]) }}" title="">
                                                    {{$single->publish_date}}
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="entry-post">
                                            <p>{!! $single->meta('excerpt') !!}</p>
                                            <div class="more-link">
                                                <a href="{{ path_for('page', ['permalink' => $single->permalink]) }}" title="">Leggi di pi&ugrave;
                                                    <span>
													<img src="images/icons/right-2.png" alt="">
												</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            <hr />
                        @endforeach
                    </div><!-- /.post-wrap -->
                    <div class="blog-pagination">
                        <ul class="flat-pagination">
                            <li class="prev">
                                <a href="#" title="">
                                    <img src="images/icons/left-1.png" alt="">Prev Page
                                </a>
                            </li>
                            <li>
                                <a href="#" class="waves-effect waves-teal" title="">01</a>
                            </li>
                            <li>
                                <a href="#" class="waves-effect waves-teal" title="">02</a>
                            </li>
                            <li class="active">
                                <a href="#" class="waves-effect waves-teal" title="">03</a>
                            </li>
                            <li>
                                <a href="#" class="waves-effect waves-teal" title="">04</a>
                            </li>
                            <li class="next">
                                <a href="#" title="">
                                    Next Page<img src="images/icons/right-1.png" alt="">
                                </a>
                            </li>
                        </ul><!-- /.flat-pagination -->
                    </div><!-- /.blog-pagination -->
                </div><!-- /.col-lg-9 col-md-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.main-blog -->






@endsection



