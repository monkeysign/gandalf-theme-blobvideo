@extends('inc.layout')

@section('content')
    <!-- /.flat-breadcrumb
    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class='trail-end'>
                            <a href="{{ path_for('wishlist') }}" title="">{{$breadcrumb[0]}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section> -->

    <main id="shop">
        <section class="flat-wishlist">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 ">
                        @include('inc.userbar')
                    </div><!-- /.col-md-4 -->
                    <div class="col-md-9">
                        <div class="wishlist">
                            <div class="title">
                                <h3>My wishlist</h3>
                            </div>
                            <div class="wishlist-content">
                                <div class="sort-box">
                                    @foreach($products as $product)
                                        <div class="product-box style3 wish_{{$product->id}}" style="height: 270px!important">
                                            <div class="imagebox style1 v3" style="position:relative">
                                                <div class="status-product">
                                                    @if ($product->state_product == 1)
                                                        <div class="usato">
                                                            Usato @if ($product->meta('usato_garantito') == 1)
                                                                Garantito @endif</div>
                                                    @endif

                                                    @if ($product->offer == 1)
                                                        <div class="offerlabel">Offerta</div>
                                                    @endif
                                                </div>

                                                <div class="box-image">
                                                    <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                       title="">
                                                        <img style="height:200px!important; width:auto!important;"
                                                             src="{{config('httpmedia').'ecommerce/prodotti/'.$product->meta('imghighlight')}}"
                                                             alt="">
                                                    </a>
                                                </div><!-- /.box-image -->
                                                <div class="box-content">
                                                    <div class="cat-name">
                                                        <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                           title="">{{(isset($product->category) && $product->category->title) ? $product->category->title : "Nessuna Categoria" }}</a>
                                                    </div>
                                                    <div class="product-name">
                                                        <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}"
                                                           title="">{{$product->title}}</a>
                                                    </div>
                                                    <div class="status">
                                                        @if($product->state_product == 0) Stato: Nuovo <br><span
                                                                style="color:#6CBE42">Disponibilità: Sì</span>@endif
                                                        @if($product->state_product == 1) Stato: Usato <br><span
                                                                style="color:#6CBE42">Disponibilità: Sì</span>@endif
                                                        @if($product->state_product == 2) Stato: Nuovo <br><span
                                                                style="color:#F26522">Disponibilità: In Arrivo</span>@endif
                                                        @if($product->state_product == 3) Stato: Nuovo <br><span
                                                                style="color:#F26522">Disponibilità: In Rientro</span>@endif
                                                    </div>
                                                    <div class="info">
                                                        {{str_limit(strip_tags($product->description), $limit = 100, $end = '...')}}
                                                    </div>
                                                </div><!-- /.box-content -->
                                                <div class="box-price">
                                                    @if($product->state_product != 3)
                                                        <div class="price">
                                                            <span class="sale">&euro;{{($product->price_offer > 0) ? $product->get_price_offer() : $product->get_price()}}</span>
                                                            @if($product->price_offer > 0) <span
                                                                    class="regular">{{$product->get_price()}}</span> @endif
                                                        </div>
                                                    @endif
                                                    <a href="{{ path_for('product', ['permalink' => $product->getPermalink()]) }}">
                                                        <button type="button" class="mt-3" style="background: #1F4296; color:#fff; border-radius: 0;">
                                                            Vai al Prodotto
                                                        </button>
                                                    </a>
                                                    <div class="mt-3">
                                                        <a href="#" class="remove-wishlist-row danger-text" data-id="{{$product->id}}">
                                                            <img src="{{asset('assets/images/')}}icons/delete.png" alt=""> <span>Rimuovi dalla whishlist</span>
                                                        </a>
                                                    </div>
                                                </div><!-- /.box-price -->
                                            </div><!-- /.imagebox -->
                                        </div><!-- /.product-box -->
                                    @endforeach


                                    <div style="height: 9px;"></div>
                                </div>
                            </div><!-- /.wishlist-content -->
                        </div><!-- /.wishlist -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-wishlish -->
    </main><!-- /#shop -->
@endsection

@section('scripts')
    <script>
        $('#listShopForm select, #listShopForm input[name="p"], #listShopForm input[name="filp"]').on('change', function () {
            $('#listShopForm').submit();
        })

        $("a[data-page]").on("click", function (e) {
            e.preventDefault();
            var numPag = $(this).data('page');
            $("#page").val(numPag);
            $('#listShopForm').submit();
            return false;

        })

        $('.brand-s').on('click', function () {
            if (this.checked) {
                $(this).prop("disabled", false);
            } else {
                $(this).prop("disabled", true);
            }
            $('#listShopForm').submit();
        })


        $('.remove-wishlist-row').on('click', function () {
            if (confirm('Sei sicuro di voler rimuovere il prodotto dalla Wishlist?')) {
                var id_prod = $(this).data('id');
                var nuovaLista=[];
                var wishList = JSON.parse(getCookie('wishlist'));
                wishList.forEach(function (item, index, array) {
                    if (item != id_prod){
                        nuovaLista.push(item);
                    }
                });
                setCookie('wishlist', JSON.stringify(nuovaLista), 6000000);
                $('.wish_' + id_prod).remove();
            }
            return false;
        })

    </script>
@endsection