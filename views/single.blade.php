@extends('inc.layout')

@section('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="#" title="">Home</a>
                            <span><img src="images/icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-item">
                            <a href="#" title="">Shop</a>
                            <span><img src="images/icons/arrow-right.png" alt=""></span>
                        </li>
                        <li class="trail-end">
                            <a href="#" title="">Smartphones</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->

    <section class="main-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div class="post-wrap">
                        <article class="main-post single">
                            <div class="featured-post">
                                <a href="#" title="">
                                    <img src="images/blog/post-01.jpg" alt="">
                                </a>
                            </div><!-- /.featured-post -->
                            <div class="divider25"></div>
                            <div class="content-post">
                                <h3 class="title-post">
                                    <a href="#" title="">{{ the_title() }}</a>
                                </h3>
                                <ul class="meta-post">
                                    <li class="date">
                                        <a href="#" title="">
                                            {{the_post()->publish_date}}
                                        </a>
                                    </li>
                                </ul><!-- /.meta-post -->
                                <div class="entry-post">
                                    {!! the_content() !!}
                                </div><!-- /.entry-post -->
                                <div class="social-single">
                                    <span>SHARE</span>
                                    <ul class="social-list style2">
                                        <li>
                                            <a href="#" title="">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="">
                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="">
                                                <i class="fa fa-instagram" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="">
                                                <i class="fa fa-pinterest" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="">
                                                <i class="fa fa-dribbble" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="">
                                                <i class="fa fa-google" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div><!-- /.social-single -->
                            </div><!-- /.content-post -->
                        </article><!-- /.main-post single -->
                    </div><!-- /.post-wrap -->
                    <div class="blog-pagination single">
                        <ul class="flat-pagination style2">
                            <li class="prev">
                                <a href="#" title="">
                                    <img src="images/icons/left-1.png" alt="">Prev Page
                                </a>
                            </li>
                            <li class="next">
                                <a href="#" title="">
                                    Next Page<img src="images/icons/right-1.png" alt="">
                                </a>
                            </li>
                        </ul><!-- /.flat-pagination style2 -->
                    </div><!-- /.blog-pagination single -->
                </div><!-- /.col-md-8 col-lg-9 -->
                <div class="col-md-4 col-lg-3">
                    @include('inc.sidebar')
                </div><!-- /.col-md-4 col-lg-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

@endsection