@extends('inc.layout')

@section('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        @foreach($breadcrumb as $key => $voice)
                            <li class="trail-item">
                                <a href="{{ path_for('shop-page-cat', ['category' => $breadcrumbPermalink[$key]]) }}" title="">{{$voice}}</a>
                                <span><img src="{{asset('assets/images/')}}icons/arrow-right.png" alt=""></span>
                            </li>
                        @endforeach
                        <li class="trail-end">
                            <a href="#" title="">{{$product->title}}</a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->


    <section class="flat-product-detail">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="status-product" style="right:initial; left:0;">
                        @if ($product->state_product == 1)
                            <div class="usato">
                                Usato @if ($product->meta('usato_garantito') == 1)
                                    Garantito @endif</div>
                        @endif


                        @if ($product->offer == 1)
                            <div class="offerlabel">Offerta</div>
                        @endif
                    </div>

                    <div class="grade-product">
                        @if ($product->grade == 'A' || $product->grade == 'B' || $product->grade == 'AB')
                            <div class="gradeimg">
                                <img src="{{asset('assets/images/')}}icons/grado-{{strtolower($product->grade)}}.png">
                            </div>
                        @endif
                    </div>

                    <div class="mac-powerup-product">
                        @if ($product->mac_powerup == 1)
                            <div class="gradeimg">
                                <img src="{{asset('assets/images/')}}icons/mac-potenziato.png">
                            </div>
                        @endif
                    </div>

                    <div class="flexslider">
                        <ul class="slides">
                            @if($product->meta('imghighlight'))
                                <li data-thumb="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($product->meta('imghighlight')) }}">
                                    <a href='#'><img
                                                src="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($product->meta('imghighlight')) }}"
                                                alt='' class="mw-100"/></a>
                                </li>
                            @endif
                            @if($product->meta('gallery'))
                                @foreach(unserialize($product->meta('gallery')) as $media)
                                    <li data-thumb="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($media) }}">
                                        <a href='#'><img
                                                    src="{{ config('httpmedia'). 'ecommerce/prodotti/' . basename($media) }}"
                                                    alt='' class="mw-100"/></a>
                                    </li>
                                @endforeach
                            @endif
                        </ul><!-- /.slides -->
                    </div><!-- /.flexslider -->
                </div><!-- /.col-md-6 -->
                <div class="col-md-6">
                    <div class="product-detail">
                        <div class="header-detail">
                            <h4 class="name">{{$product->title}}</h4>
                            <div class="category">
                                Tipologia: {{$product->category->productType->title}}
                            </div>
                        </div><!-- /.header-detail -->
                        <div class="tab-product-feature mt-3">
                            <div class="row mb-3">
                                <div class="col-4">
                                    <div class="row">
                                        <div class="col-12">Produttore</div>
                                        <div class="col-12"><strong>{{$product->produttore}}</strong></div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="row">
                                        <div class="col-12">Stato</div>
                                        <div class="col-12"><strong>{{$product->state_product_label}}</strong></div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="row">
                                        <div class="col-12">Disponibile</div>
                                        <div class="col-12">
                                            <strong>
                                                @if($product->quantity>0)
                                                    SI
                                                @else
                                                    NO
                                                @endif
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if($attributes)
                                @php
                                    $posizione= 0;
                                    $comp = false;
                                @endphp
                                @foreach($attributes as $attributo)
                                    @if(strpos(strtolower($attributo->feature->title), 'compatibilità') === false)
                                        @if($posizione % 3 ==0)
                                            <div class="row mb-3">
                                                @endif
                                                <div class="col-4">
                                                    <div class="row">
                                                        <div class="col-12">{{$attributo->feature->title}}</div>
                                                        <div class="col-12"><strong>{{$attributo->title}}</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                    $posizione=$posizione+1;
                                                @endphp
                                                @if($posizione % 3 ==0)
                                            </div>
                                        @endif
                                    @else @php $comp = true @endphp @endif
                                @endforeach
                                @if($posizione % 3 !=0)
                        </div>
                        @endif
                        @endif

                        <div class="info-text mt-3 mb-3">
                            {!! $product->description !!}
                        </div>
                    </div>


                    <div class="footer-detail">
                        <div class="row">
                            <div class="col-md-12">
                                <form name="form-carrello" id="form-carrello" action="{{ path_for('save-carrello') }}"
                                      method="POST">
                                    <input type="hidden" name="id_product" value="{{$product->id}}"/>

                                    <div class="row ">
                                        <div class="content-detail col-8 col-md-9">
                                            @if($product->state_product != 3 && $product->quantity > 0)
                                                <div class="price">
                                                    @if(($product->offer==1) && ($product->price_offer>0))
                                                        <div class="mb-3">Prezzo di listino:
                                                            <span style="text-decoration: line-through">&euro; {{$product->price_label}}</span></div>
                                                        <div>
                                                            <span class="sale">&euro; {{$product->price_offer_label}}</span>
                                                        </div>
                                                    @else
                                                        <span class="sale">&euro; {{$product->price_label}}</span>
                                                    @endif
                                                </div>
                                            @endif
                                        </div><!-- /.content-detail -->

                                        <div class="col-4 col-md-3">
                                            @if($product->brand)
                                                @if($product->brand->meta('imghighlight'))
                                                    <img style="margin-top: @if(($product->offer==1) && ($product->price_offer>0)) {{'1rem;'}} @else {{'0;'}} @endif"
                                                         src="{{ config('httpmedia'). 'ecommerce/brand/' . $product->brand->meta('imghighlight')}}"
                                                         class="w-100">
                                                @endif
                                            @endif
                                        </div>

                                        <input type="hidden" name="quantity" value="1" min="1" max="100" required
                                               placeholder="Quantità">
                                        {{--<div class="quanlity col-md-6 col-form">--}}
                                        {{--<span class="btn-down"></span>--}}
                                        {{----}}
                                        {{--<span class="btn-up"></span>--}}
                                        {{--</div>--}}
                                    </div>


                                    <div class="box-cart style2">
                                        @if($product->state_product < 2 && $product->quantity > 0)
                                            <div class="btn-add-cart">
                                                <a href="#" onclick="return false;" title="" id="add-to-cart"><img
                                                            src="{{asset('assets/images/')}}icons/add-cart.png" alt="">Aggiungi
                                                    al
                                                    Carrello</a>
                                            </div>
                                        @else
                                            <div class="price"><span
                                                        class="sale">{!! $product->meta('arrival_date') !!}</span></div>
                                        @endif
                                        {{--<div class="compare-wishlist">--}}
                                        {{--<a href="compare.html" class="wishlist" title=""><img--}}
                                        {{--src="{{asset('assets/images/')}}icons/wishlist.png" alt="">Wishlist</a>--}}
                                        {{--</div>--}}
                                            @if(user_logged())
                                                <div class="compare-wishlist">
                                                    <a href="#" class="wishlist link-wishlist wish_{{$product->id}}" title="" data-idwish="{{$product->id}}">
                                                        <i class="fa fa-heart"> </i> <span>Lista dei desideri</span>
                                                    </a>
                                                </div>
                                            @endif

                                        <div class="response-form">
                                        </div>
                                    </div><!-- /.box-cart -->
                                </form>
                            </div>
                        </div>
                        <div class="social-single">

                            <h4 class="mb-4">Condividi su:</h4>
                            <!-- AddToAny BEGIN -->
                            <!-- AddToAny BEGIN -->
                            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_twitter"></a>
                                <a class="a2a_button_google_plus"></a>
                                <a class="a2a_button_whatsapp"></a>
                                <a class="a2a_button_telegram"></a>
                            </div>
                            <script>
                                var a2a_config = a2a_config || {};
                                a2a_config.locale = "it";
                            </script>
                            <script async src="https://static.addtoany.com/menu/page.js"></script>
                            <!-- AddToAny END -->
                            <!-- AddToAny END -->
                            <!-- AddToAny END -->
                        </div><!-- /.social-single -->
                    </div><!-- /.footer-detail -->
                </div><!-- /.product-detail -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-product-detail -->

    <section class="flat-product-content">
        <ul class="product-detail-bar align-content-start">
            <li class="active">Descrizione</li>
            @if(trim(strip_tags($product->meta('techincalnote'))) != '')
                <li>Specifiche Tecniche</li>@endif
            @if(trim(strip_tags($product->meta('boxcontent'))) != '')
                <li>Contenuto della Confezione</li>@endif
            @if($comp == true)
                <li>Compatibilit&agrave;</li>@endif
        </ul><!-- /.product-detail-bar -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="description-text">
                        {!! $product->note !!}
                    </div><!-- /.description-text -->
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
            @if(trim(strip_tags($product->meta('techincalnote'))) != '')
                <div class="row">
                    <div class="col-md-12">
                        <div class="tecnical-specs">
                            {!! $product->meta('techincalnote') !!}
                        </div><!-- /.tecnical-specs -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->@endif
            @if(trim(strip_tags($product->meta('boxcontent'))) != '')
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-specs">
                            {!! $product->meta('boxcontent') !!}
                        </div><!-- /.tecnical-specs -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->@endif
            @if($comp == true)
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-specs">
                            <strong>Compatibile con:</strong><br>
                            @foreach($attributes as $attributo)
                                @if(strpos(strtolower($attributo->feature->title), 'compatibilità') !== false)
                                    <span>{{$attributo->title}}</span><br>
                                @endif
                            @endforeach
                        </div><!-- /.tecnical-specs -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            @endif
        </div><!-- /.container -->

    </section><!-- /.flat-product-content -->

    <section class="flat-row flat-iconbox style3">
        <div class="container">
            <div class="row">
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/banconota.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Spedizioni Gratis</h4>
                                <p>per ordini superiori a 79&euro;</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/consegna.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Consegne 24/48 H</h4>
                                <p>con corriere espresso</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img style="width:45px;" src="{{asset('assets/images/')}}icons/pagamentisicuri.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Pagamenti Sicuri</h4>
                                <p>con PayPal e Bonifico</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/utente.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Registrati al Sito</h4>
                                <p>per Offerte e Promozioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
                <div class="col-xs-2-5">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="image">
                                <img src="{{asset('assets/images/')}}icons/fumetto.svg" alt="">
                            </div>
                            <div class="box-title">
                                <h4>Contattaci</h4>
                                <p>per maggiori informazioni</p>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.box-header -->
                    </div><!-- /.iconbox -->
                </div><!-- /.col-lg-3 col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-iconbox -->




    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Prodotto inserito correttamente al carrello</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="box-cart style2">
                        <div class="btn-add-cart">
                            <a href="{{ path_for('checkout') }}" title=""><img
                                        src="{{asset('assets/images/')}}icons/add-cart.png" alt="">Vai al Carrello</a>
                        </div>
                        <div class="btn-add-cart btn-add-cart-modal-close">
                            <a href="#" data-dismiss="modal" aria-label="Close">Chiudi</a>
                        </div>
                        <div class="response-form">
                        </div>
                    </div><!-- /.box-cart -->

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
@endsection



@section('scripts')

    <script>
        $(function () {

            $("#add-to-cart").click(function () {
                $("#form-carrello").submit();
            });
            $("#form-carrello").validate({
                submitHandler: function (form) {
                    $('.response').removeClass('btn-success btn-danger').addClass('btn btn-default').html("Operazione in corso");
                    $('#' + form.id).ajaxSubmit({
                        success: showResponse,
                        dataType: "json"
                    });
                    return false;
                },
                errorClass: "help-block",
                errorElement: "div",
                rules: {
                    quantity: {required: !0}
                },
                messages: {
                    quantity: "Inserisci la quantità"
                },
                errorPlacement: function (e, t) {
                    t.parents(".col-form").append(e)
                },
                highlight: function (e) {
                    $(e).closest(".col-form").removeClass("has-success has-error ").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function (e) {
                    e.closest(".col-form").removeClass("has-success has-error mt-5").addClass("has-success"), e.closest(".help-block").remove();
                }
            });

        });

        function showResponse(responseText, statusText, xhr, $form) {
            var res = responseText.result;
            if (!isNaN(res)) {
                $('#exampleModal').modal();
            } else {
                $('.response-form').removeClass('btn btn-success mt-5').addClass('btn btn-danger mt-5').html("<p>Errore carrello: " + res + "</p>");
            }
        }


    </script>
@endsection
