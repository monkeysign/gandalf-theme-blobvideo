<div id="mobilemenu" class="notclose">
    <div class="menuToggle" style="width: 26px; height: 26px; position:absolute; left:10px; top:10px; text-align: center; color:#fff; background: #2A2929 ; z-index:10; border:1px solid #2A2929;">
        <i class="fa fa-close"></i>
    </div>
    <div class="wrappermenu notclose">
        <div class="row">
            <div class="col-12 text-center">
                <ul class="flat-unstyled">
                    @if(!user_logged())
                        <li class="account">
                            <a href="{{path_for('loginp',array())}}" title="">
                                <img width="20" style="margin-top:-2px;"
                                     src="{{asset('assets/images/')}}icons/utente-2.svg">
                                Accedi</a>
                        </li>
                        <li>
                            <a href="{{ path_for('registrationp', array()) }}" title="">
                                <img width="20"
                                     style="margin-top:-2px;"
                                     src="{{asset('assets/images/')}}icons/utente-add-2.svg">
                                Registrati</a>
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            Ciao {{user_logged()->name}}
                        </li>
                @endif
                <!--
                            <li>
                                <a href="{{ path_for('checkout') }}" title=""><i class="fa fa-heart mr-1"></i> Lista dei
                                    desideri</a>
                            </li>-->
                </ul><!-- /.flat-unstyled -->
            </div>

            <div class="col-12">
                <ul class="menu">
                    <li>
                        <a data-href="{{path_for('frontpage')}}" title="" class="dropdown">
                            <span class="menu-title">Home</span>
                        </a>
                    </li>

                    <li style="background: #DD1C1A; color:#fff;">
                        <a data-href="/offerte" title="" class="dropdown">
                            {{--<span class="menu-img">--}}
                                {{--<img src="{{asset('assets/images/')}}icons/nataleicon.png" style="width: 20px; height:20px; display: inline-block; margin-right:5px; position: relative; top:-3px;">--}}
                            {{--</span>--}}
                            <span class="menu-title">Offerte</span>
                        </a>
                    </li>

                    <li>
                        <a title="#" class="dropdown">
                            <span class="menu-title">Categorie Prodotti</span>
                        </a>
                        <span class="btn-dropdown" style="float: right; margin-right:10px;"><i
                                    class="fa fa-caret-right"></i></span>
                        <ul class="drop-menu">
                            @if($categorieRoot)
                                @foreach($categorieRoot as $categoriaRoot)
                                    <li>
                                        <a data-href="{{ path_for('shop-page-cat', ['category' => $categoriaRoot->permalink]) }}"
                                           title="" class="dropdown">
												<span class="menu-img">
													<img src="{{config('httpmedia')."ecommerce/categorie/".$categoriaRoot->meta('imgicon')}}"
                                                         style="width: 30px; height:30px;">
												</span>
                                            <span class="menu-title">
													{{$categoriaRoot->title}}
												</span>
                                        </a>
                                        <span class="btn-dropdown" style="float: right; margin-right:10px;"><i
                                                    class="fa fa-caret-right"></i></span>

                                        @if(count($categoriaRoot->categoryChild)>0)
                                            <ul class="drop-menu">
                                                @foreach($categoriaRoot->categoryChild as $catLivUno)
                                                    <li>

                                                        <a class="dropdown"
                                                           data-href="{{ path_for('shop-page-cat', ['category' => $catLivUno->permalink]) }}">
                                                    <span class="menu-title">
													{{$catLivUno->title}}
												    </span>
                                                        </a>

                                                        {{--@if($catLivUno->categoryChild && count($catLivUno->categoryChild)>0)--}}
                                                        {{--<span class="btn-dropdown" style="float: right;"><i--}}
                                                        {{--class="fa fa-caret-right"></i></span>--}}

                                                        {{--<ul class="drop-menu">--}}
                                                        {{--@if($catLivUno->categoryChild && count($catLivUno->categoryChild)>0)--}}
                                                        {{--@foreach($catLivUno->categoryChild as $catLivDue)--}}
                                                        {{--<li>--}}
                                                        {{--<a href="{{ path_for('shop-page-cat', ['category' => $catLivDue->id]) }}">{{$catLivDue->title}}</a>--}}
                                                        {{--</li>--}}
                                                        {{--@endforeach--}}
                                                        {{--@endif--}}
                                                        {{--</ul>--}}
                                                        {{--@endif--}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                    <li>
                        <a data-href="{{ path_for('single-page', ['permalink' => 'chi-siamo']) }}" title="">Chi
                            Siamo</a>
                    </li>
                    <li>
                        <a data-href="{{path_for('frontpage')}}" title="" class="dropdown">
                            <span class="menu-title">Specialisti dell'usato</span>
                        </a>
                        <span class="btn-dropdown" style="float: right; margin-right:10px;"><i
                                    class="fa fa-caret-right"></i></span>
                        <ul class="drop-menu">
                            <li>
                                <a data-href="{{ path_for('single-page', ['permalink' => 'usato-gaming']) }}"
                                   title="">Usato Gaming</a>
                            </li>
                            <li>
                                <a data-href="{{ path_for('single-page', ['permalink' => 'usato-apple']) }}"
                                   title="">Usato Apple</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" title="" class="dropdown">
                            <span class="menu-title">Assistenza Specializzata</span>
                        </a>
                        <span class="btn-dropdown" style="float: right; margin-right:10px;"><i
                                    class="fa fa-caret-right"></i></span>
                        <ul class="drop-menu">
                            <li>
                                <a data-href="{{ path_for('single-page', ['permalink' => 'assistenza-console']) }}"
                                   title="">Console Gaming</a>
                            </li>
                            <li>
                                <a data-href="{{ path_for('single-page', ['permalink' => 'assistenza-apple']) }}"
                                   title="">iPhone, iPad e Mac</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a data-href="/blog" title="" class="dropdown">
                            <span class="menu-title">Blog</span>
                        </a>
                    </li>
                    <li>
                        <a data-href="{{ path_for('single-page', ['permalink' => 'contatti']) }}" title="" class="dropdown">
                            <span class="menu-title">Contatti</span>
                        </a>
                    </li>


                    @if(user_logged())
                        <li style="background: #e6e6e6">
                            <a data-href="{{path_for('area-user')}}" title="" class="dropdown">
                                <span class="menu-title">Area Utente</span>
                            </a>
                        </li>
                        <li data-style="background: #e6e6e6">
                            <a href="{{path_for('logout')}}" title="" class="dropdown">
                                <span class="menu-title">Esci</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>