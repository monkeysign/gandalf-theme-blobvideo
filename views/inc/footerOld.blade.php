<footer>
    <div class="p-3 text-center">
        <p class="m-0">{{config('appname')}} - {{config('appdescription')}} </p>
    </div>
</footer>