<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
?>
        <!doctype html>
<html lang="{{config('locale')}}">

<head>

    <!-- Global site tag (gtag.js) - Google Analytics →-->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47138102-1"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-47138102-1');
    </script>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<!--
    @if(isset($post) and $post)
    {{hooks()->do_action(THEME_HEADER_SEO, ['post' => $post])}}
@else
    {{hooks()->do_action(THEME_HEADER_SEO, null)}}
            @endif-->

        <meta name="author" content="Monkeysign">

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Boostrap style -->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">

        <!-- Theme style -->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">

        <!-- Reponsive -->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}">

    <!--<link rel="shortcut icon" href="{{asset('assets/css/app.css')}}favicon/favicon.png">-->


        <!-- Bootstrap CSS -->
        <!--
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
              integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">
    @yield('style')

    <!-- <meta property="og:type" content="article" />-->
        @if($product and isset($singleProduct))
            <meta property="og:title" content="{{$product->title}}">
            <meta property="og:description" content="{{ strip_tags($description) }}">
            <meta property="og:url" content="{{$url}}">
            <meta property="product:brand" content="{{$product->produttore}}">
            <meta property="product:price:currency" content="EUR">
            <meta property="product:retailer_item_id" content="{{$product->id}}">
            <meta property="product:price:amount" content="<?= $product->offer==1 && $product->price_offer>0 ? $product->price_offer : $product->price ?>">
            <meta property="product:condition"
                  content="<?= $product->state_product == 1 ? "used" : "used" ?>">
            <meta property="product:availability" content="<?= $product->quantity > 0 ? "in stock" : "out of stock" ?>">
            @else
            <meta property="og:title" content="{{$titleFacebook}}">
        @endif

        <title>{{ $title }}</title>

        <meta property="og:image" content="{{$imgFacebook}}">
        <meta name="description" content="{{ strip_tags($description) }}">

        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '986218368097267');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none"
                 src="https://www.facebook.com/tr?id=986218368097267&ev=PageView&noscript=1"/>
        </noscript>
</head>
<body class="header_sticky">
@include('inc.mobilemenu')
@include('inc.header')

@yield('content')

@include('inc.footer')

{{--
<main class="p-5 bg-white">
    <div class="container">
        <h1 class="text-center">Sito in costruzione </h1>
    </div>
</main>--}}

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
-->

<!-- Javascript -->
<script type="text/javascript" src="{{asset('assets/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/tether.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery.circlechart.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/easing.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery.zoom.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery.flexslider-min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/owl.carousel.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/js/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery.mCustomScrollbar.js')}}"></script>
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtRmXKclfDp20TvfQnpgXSDPjut14x5wk&region=GB"></script>
<script type="text/javascript" src="{{asset('assets/js/gmap3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/waves.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery.countdown.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/js/main.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery.form.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/function.js')}}"></script>

<script>
    var arrayCatSearch = [];
    var delayTimer;
    var arrayWishList = [];
    $(function () {
        var listaCookieWish = JSON.parse(getCookie('wishlist'));
        listaCookieWish.forEach(function (item, index, array) {
            $('.wish_' + item).addClass('wishListActive');
        });

        $(".link-wishlist").click(function () {
            var newWishList = [];
            var idProdotto = $(this).data('idwish');
            var cookieWish = JSON.parse(getCookie('wishlist'));
            if (cookieWish.indexOf(idProdotto) == -1) {
                $('.wish_' + idProdotto).addClass('wishListActive');
                cookieWish.push(idProdotto);
                setCookie('wishlist', JSON.stringify(cookieWish), 6000000);
            } else {
                $('.wish_' + idProdotto).removeClass('wishListActive');
                //mi appresto ad eliminare  il prodotto nella wish list
                cookieWish.forEach(function (item, index, array) {
                    if (item != idProdotto) {
                        newWishList.push(item);
                    }
                });
                setCookie('wishlist', JSON.stringify(newWishList), 6000000);
            }
            return false;
        });


        $('.menuToggle').on('click', function () {
            $('#mobilemenu').toggleClass('visibile');
            return false;
        })

        $('body').on('click', function (e) {
            if ($('#mobilemenu').hasClass('visibile')) {
                $('#mobilemenu').removeClass('visibile');
                e.preventDefault();
                return false;
            }
        })
        $('#mobilemenu').click(function (e) {
            e.stopPropagation();
        })

        $('#mobilemenu .menu li').click(function (e) {
            var $dropdown = $(this).children('.btn-dropdown');
            console.log($dropdown);
            if ($dropdown.length > 0) {
                if (!$dropdown.hasClass('opened')) {
                    $dropdown.siblings('.drop-menu').show(150);
                } else {
                    $dropdown.siblings('.drop-menu').hide(150);
                }
                $dropdown.toggleClass('opened')
                $dropdown.children('i').toggleClass('fa-caret-right');
                $dropdown.children('i').toggleClass('fa-caret-down');
                e.stopPropagation();
            } else {
                location.href = $(this).children('a').data('href');
            }
        })

        $('.result-ajax-search .loading, .result-ajax-search-cat .loading').hide();

        $("li[data-idcateg]").on("click", function (e) {
            //$(this).toggleClass('li-active');
            var $id = $(this).data('idcateg');
            var $title = $(this).data('title');
            if ($title !== 'Tutte') {
                $(".selctfake div").html($title);
                $('.all-categories').removeClass("show");
                var find = false;
                arrayCatSearch = [];
                arrayCatSearch = arrayCatSearch.filter(function (value) {
                    if (value !== $id) return value;
                    else find = true;
                });
                if (!find) arrayCatSearch.push($id);
                $("#searchCat").val(arrayCatSearch.join());
                $('#srcAjax').keyup();
            } else {
                $(".selctfake div").html('Tutte');
                $('.all-categories').removeClass("show");
            }
            return false;
        })

        /*
                $('#srcAjax').on('keyup', function () {
                    var id_category=0;
                    if ($(this).val().length > 2) {
                        $('.result-ajax-search .loading, .result-ajax-search-cat .loading').show();
                        $('.result-ajax-search .nothing, .result-ajax-search-cat .nothing').hide();
                        $.ajax({
                            method: "GET",
                            url: '{{ path_for('ajax-search') }}',
                    data: {s: $(this).val(), cat :("#searchCat").val()},
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        $('.result-ajax-search .result, .result-ajax-search-cat .result').remove();
                        $('.result-ajax-search .loading, .result-ajax-search-cat .loading').hide();
                        if (data.products.length > 0) {
                            data.products.map(function (product) {

                                if(product.price_offer > 0){product.price = product.price_offer}

                                var html = `<li class="result">
                                                <div class="image">
                                                    <img src="${product.image}" alt="">
                                                </div>
                                                <div class="info-product">
                                                    <div class="name">
                                                        <a href="{{ path_for('product', ['permalink' => '' ]) }}/${product.permalink}" title="">${product.title}</a>
                                                    </div>
                                                    <div class="price">
                                                        <span class="sale">
																		&euro;${parseFloat(Math.round(product.price * 100) / 100).toFixed(2)}
																	</span>
                                                    </div>
                                                </div>
                                            </li>`;
                                $('.result-ajax-search').append(html);
                            })
                        } else {
                            $('.result-ajax-search .nothing').show();
                        }

                        if (data.category.length > 0) {
                            data.category.map(function (cat) {
                                var html = `<li class="result">
                            <a href="{{ path_for('shop-page-cat', ['category' => '']) }}/${cat.id}">${cat.title}</a>
                            </li>`;
                                $('.result-ajax-search-cat').append(html);
                            })
                        } else {
                            $('.result-ajax-search-cat .nothing').show();
                        }
                    }
                })
            } else {
                $('.result-ajax-search .loading, .result-ajax-search-cat .loading').hide();
                $('.result-ajax-search .nothing, .result-ajax-search-cat .nothing').show();
            }
        })*/

        function delay(callback, ms) {
            var timer = 0;
            return function () {
                var context = this, args = arguments;
                clearTimeout(timer);
                timer = setTimeout(function () {
                    callback.apply(context, args);
                }, ms || 0);
            };
        }


        $('#srcAjax').on('keyup', delay(function () {
            $('.result-ajax-search .result').remove();
            $('.result-ajax-search .loading').show();
            $('.result-ajax-search .nothing').hide();
            $.ajax({
                method: "GET",
                url: '{{ path_for('ajax-search') }}',
                data: {s: $(this).val(), tipoligie: $("#searchCat").val()},
                dataType: "json",
                cache: false,
                success: function (data) {
                    $('.result-ajax-search .result').remove();
                    $('.result-ajax-search .loading').hide();
                    if (data.products.length > 0) {
                        data.products.map(function (product) {

                            if (product.price_offer > 0) {
                                product.price = product.price_offer
                            }

                            var html = `<li class="result">
                                                <div class="image">
                                                    <a href="{{ path_for('product', ['permalink' => '' ]) }}/${product.permalink}" title=""><img src="${product.image}" alt=""></a>
                                                </div>
                                                <div class="info-product">
                                                    <div class="name">
                                                        <a href="{{ path_for('product', ['permalink' => '' ]) }}/${product.permalink}" title="">${product.title}</a>
                                                    </div>
                                                    <div class="price">
                                                        <span class="sale">
																		&euro;${parseFloat(Math.round(product.price * 100) / 100).toFixed(2)}
																	</span>
                                                    </div>
                                                </div>
                                            </li>`;
                            $('.result-ajax-search').append(html);
                        })
                    } else {
                        $('.result-ajax-search .nothing').show();
                    }
                }
            })
        }, 500))
    })
</script>

<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            facebook: "150963771608217", // Facebook page ID
            whatsapp: "+390815152511", // WhatsApp number
            call_to_action: "Scrivici", // Call to action
            button_color: "#129BF4", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,whatsapp", // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->
@yield('scripts')

</body>
</html>