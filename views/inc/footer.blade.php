<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="widget-ft widget-about">
                    <div class="widget-title">
                        <h3>Ultime dal Blog</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="mb-3"><a target="_blank" href="{{$blog_link}}">{{$blog_title}}</a></h4>
                            <p>{{$blog_description }}</p>
                        </div>
                    </div><!-- /.logo-ft -->
                </div><!-- /.widget-about -->
            </div><!-- /.col-lg-3 col-md-6 -->
            <div class="col-lg-4 col-md-6">
                <div class="widget-ft widget-newsletter">
                    <div class="widget-title">
                        <h3>Iscriviti alla Newsletter</h3>
                    </div>
                    <p>Non perdere le nostre novit&agrave;
                    </p>

                    <form action="{{ path_for('subscribe-mailchimp') }}" id="subscribe-form" class="subscribe-form" method="post" accept-charset="utf-8">
                        <div class="subscribe-content col-form">
                            <input type="text" name="subscribe_email" class="subscribe_email" placeholder="La Tua E-Mail">
                            <button type="submit"><img src="{{asset('assets/images/')}}icons/right-2.png" alt=""></button>
                        </div>
                    </form><!-- /.subscribe-form -->
                    <span class="response-subscribe_email"></span>
                </div><!-- /.widget-newsletter -->
            </div><!-- /.col-lg-4 col-md-6 -->
            <div class="col-lg-4 col-md-4">
                <div class="widget-ft widget-newsletter">
                    <div class="widget-title">
                        <h3>Seguici su</h3>
                    </div>
                    <ul class="social-list">
                        <li>
                            <a href="https://www.facebook.com/BlobVideo" title="facebook" target="u_blank">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/BlobVideo" title="" target="u_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/blobvideo/" title="instagram" target="u_blank">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/user/BlobVideoTV" title="youtube" target="u_blank">
                                <i class="fa fa-youtube" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/+BlobVideoPagani" title="google plus" target="u_blank">
                                <i class="fa fa-google" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul><!-- /.social-list -->
                </div><!-- /.widget-newsletter -->
            </div><!-- /.col-lg-4 col-md-6 -->
        </div>
        <div class="row mt-5">
            <div class="col-lg-4 col-md-6">
                <div class="widget-ft widget-about">
                    <div class="logo logo-ft">
                        <a href="index.html" title="">
                            <img src="{{asset('assets/images/')}}icons/logo-blob-video-footer.png" alt="">
                        </a>
                    </div><!-- /.logo-ft -->
                    <div class="widget-content" style="margin-top:-15px;">
                        {{--<div class="icon">--}}
                            {{--<img src="{{asset('assets/images/')}}icons/telefono.svg" alt="">--}}
                        {{--</div>--}}
                        <div class="info" style="padding:0; margin:0;">
                            <p class="phone"></p>
                            <p class="address">
                                Via Alcide De Gasperi 215/217,
                                <br />Pagani (SA)<br />
                                081 5152511

                            </p>
                        </div>
                    </div><!-- /.widget-content -->
                </div><!-- /.widget-about -->
            </div><!-- /.col-lg-3 col-md-6 -->
            <div class="col-lg-4 col-md-6">
                <div class="widget-ft  widget-menu">
                    <div class="widget-title">
                        <h3>Account</h3>
                    </div>
                    <ul>

                        <li>   <a href="{{path_for('area-user-impostazioni')}}" title="">Gestione Account</a></li>
                        <li>   <a href="{{path_for('user-save-order', ['type' => 1])}}" title="">I Tuoi Ordini</a></li>
                        <li>   <a href="{{ path_for('checkout') }}" title="">Checkout</a></li>

                    </ul><!-- /.cat-list-ft -->
                </div><!-- /.widget-categories-ft -->
            </div><!-- /.col-lg-3 col-md-6 -->
            <div class="col-lg-4 col-md-6">
                <div class="widget-ft widget-menu">
                    <div class="widget-title">
                        <h3>Informazioni Utili</h3>
                    </div>
                    <ul>
                        <li> <a href="{{ path_for('single-page', ['permalink' => 'terminiecondizioni']) }}" title="" >Termini e Condizioni </a> </li>
                        <li> <a href="{{ path_for('single-page', ['permalink' => 'spedizioni']) }}" title="" > Metodi e Costi di Spedizione </a> </li>
                        <li> <a href="{{ path_for('single-page', ['permalink' => 'privacy']) }}" title="" > Privacy e Cookie Policy </a> </li>
                    </ul>
                </div><!-- /.widget-menu -->
            </div><!-- /.col-lg-2 col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</footer><!-- /footer -->

<section class="footer-bottom style3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="copyright"> © Blob Video - Tutti i diritti riservati</p>
                <div class="btn-scroll">
                    {{--<ul class="pay-list">--}}
                        {{--<li>--}}
                            {{--<a href="#" title="">--}}
                                {{--<img src="{{asset('assets/images/')}}logos/ft-01.png" alt="">--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#" title="">--}}
                                {{--<img src="{{asset('assets/images/')}}logos/ft-02.png" alt="">--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#" title="">--}}
                                {{--<img src="{{asset('assets/images/')}}logos/ft-03.png" alt="">--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#" title="">--}}
                                {{--<img src="{{asset('assets/images/')}}logos/ft-04.png" alt="">--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#" title="">--}}
                                {{--<img src="{{asset('assets/images/')}}logos/ft-05.png" alt="">--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul><!-- /.pay-list -->--}}
                    <a href="#" title="">
                        <img src="{{asset('assets/images/')}}icons/top.png" alt="">
                    </a>
                </div><!-- /.btn-scroll -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.footer-bottom -->

</div><!-- /.boxed -->

<div class="modal fade" id="exampleModalSubscribe" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Iscrizione Newsletter confermata</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                    <a href="#" data-dismiss="modal" aria-label="Close">&times;</a>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


@section('scripts')

    <script>
        $(function () {

            $('.btn.dropdown-mobile').on('click',function(){})

            $("#subscribe-form").validate({
                submitHandler: function(form) {
                    $('.response-subscribe_email').removeClass('btn-success btn-danger').addClass('btn btn-default').html("Operazione in corso");
                    $('#' + form.id).ajaxSubmit({
                        success: showResponseMail,
                        dataType: "json"
                    });
                    return false;
                },
                errorClass: "help-block",
                errorElement: "div",
                rules: {
                    subscribe_email: {required: !0, email: !0}
                },
                messages: {
                    subscribe_email: "Inserisci una mail valida"
                },
                errorPlacement: function(e, t) {
                    t.parents(".col-form").append(e)
                },
                highlight: function(e) {
                    $(e).closest(".col-form").removeClass("has-success has-error ").addClass("has-error"), $(e).closest(".help-block").remove()
                },
                success: function(e) {
                    e.closest(".col-form").removeClass("has-success has-error mt-5").addClass("has-success"), e.closest(".help-block").remove();
                }
            });
        });

        function showResponseMail(responseText, statusText, xhr, $form) {
            var res = responseText.result;
            if (res==1) {
                $('#exampleModalSubscribe').modal();
                $('.response-subscribe_email').removeClass('btn-success btn-danger').html("");
            } else {
                $('.response-subscribe_email').removeClass('btn btn-success mt-5').addClass('btn btn-danger mt-5').html("<p>Errore Iscrizione Newsletter, si prega di riprovare</p>");
            }
        }

    </script>

@endsection



