<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{ path_for('frontpage')}}">{{config('appname')}}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ path_for('page', [ 'permalink' => 'about' ])}}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{ path_for('archive', [ 'code_post_type' => 'articoli', 'code_cat' => '' ])}}">Articoli</a>
                </li>
                @if(!user_logged())
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="{{path_for('loginp',array())}}">Accedi</a>
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Ciao {{user_logged()->name}}
                        </a>
                        <div class="dropdown-menu p-3" aria-labelledby="navbarDropdownMenuLink">
                            <h5>Ciao {{user_logged()->name}}</h5>
                            <p>
                                <a href="{{path_for('logout')}}?ref={{request()->path()}}">Logout</a>
                                @if(user_logged()->role == 0) | <a href="{{path_for('admin.dashboard')}}">Admin</a>@endif
                            </p>
                        </div>
                        @endif
                    </li>
            </ul>
            <form class="form-inline ml-auto" method="GET" action="{{path_for('search')}}">
                <input required class="form-control mr-sm-2" type="search" name="q" placeholder="Cerca nel sito..." aria-label="Search">
                <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Cerca</button>
            </form>

        </div>

    </nav>
</header>