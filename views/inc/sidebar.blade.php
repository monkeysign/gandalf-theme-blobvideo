<div class="sidebar left">
    <div class="widget widget-search">
        <form action="#" method="get" accept-charset="utf-8">
            <input type="text" name="widget-search" placeholder="Search">
        </form>
    </div><!-- /.widget widget-search -->
    <div class="widget widget-categories">
        <div class="widget-title">
            <h3>Categories</h3>
        </div>
        <ul class="cat-list">
            <li>
                <a href="#" title="">Accessories<span>(03)</span></a>
            </li>
            <li>
                <a href="#" title="">Cameras<span>(19)</span></a>
            </li>
            <li>
                <a href="#" title="">Computers<span>(56)</span></a>
            </li>
            <li>
                <a href="#" title="">Laptops<span>(03)</span></a>
            </li>
            <li>
                <a href="#" title="">Networking<span>(03)</span></a>
            </li>
            <li>
                <a href="#" title="">Old Products<span>(89)</span></a>
            </li>
            <li>
                <a href="#" title="">Smartphones<span>(90)</span></a>
            </li>
            <li>
                <a href="#" title="">Software<span>(23)</span></a>
            </li>
        </ul>
    </div><!-- /.widget widget-categories -->
    <div class="widget widget-products">
        <div class="widget-title">
            <h3>Latest Products</h3>
        </div>
        <ul class="product-list">
            <li>
                <div class="img-product">
                    <a href="#" title="">
                        <img src="images/blog/14.jpg" alt="">
                    </a>
                </div>
                <div class="info-product">
                    <div class="name">
                        <a href="#" title="">Razer RZ02-01071 <br />500-R3M1</a>
                    </div>
                    <div class="queue">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </div>
                    <div class="price">
                        <span class="sale">$50.00</span>
                        <span class="regular">$2,999.00</span>
                    </div>
                </div>
            </li>
            <li>
                <div class="img-product">
                    <a href="#" title="">
                        <img src="images/blog/13.jpg" alt="">
                    </a>
                </div>
                <div class="info-product">
                    <div class="name">
                        <a href="#" title="">Notebook Black Spire <br />V Nitro VN7-591G</a>
                    </div>
                    <div class="queue">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </div>
                    <div class="price">
                        <span class="sale">$24.00</span>
                        <span class="regular">$2,999.00</span>
                    </div>
                </div>
            </li>
            <li>

                <div class="img-product">
                    <a href="#" title="">
                        <img src="images/blog/12.jpg" alt="">
                    </a>
                </div>
                <div class="info-product">
                    <div class="name">
                        <a href="#" title="">Apple iPad Mini <br />G2356</a>
                    </div>
                    <div class="queue">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </div>
                    <div class="price">
                        <span class="sale">$90.00</span>
                        <span class="regular">$2,999.00</span>
                    </div>
                </div>
            </li>
        </ul>
    </div><!-- /.widget widget-products -->
    <div class="widget widget-tags">
        <div class="widget-title">
            <h3>Popular Tags</h3>
        </div>
        <ul class="tag-list">
            <li>
                <a href="#" class="waves-effect waves-teal" title="">Phone</a>
            </li>
            <li>
                <a href="#" class="waves-effect waves-teal" title="">Cameras</a>
            </li>
            <li>
                <a href="#" class="waves-effect waves-teal" title="">Computers</a>
            </li>
            <li>
                <a href="#" class="waves-effect waves-teal" title="">Laptops</a>
            </li>
            <li>
                <a href="#" class="waves-effect waves-teal" title="">Headphones</a>
            </li>
        </ul>
    </div><!-- /.widget widget-tags -->
</div><!-- /.sidebar left -->