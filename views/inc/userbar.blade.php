<div class="sidebar left">
    <div class="widget widget-categories">
        <div class="widget-title">
            <h3>Men&ugrave; Utente</h3>
        </div>
        <ul class="cat-list">
            <li>    <a href="{{path_for('area-user')}}" title="">Bacheca</a></li>
            <li>    <a href="{{path_for('wishlist')}}" title="">La Mia Wishlist</a></li>
            <li>   <a href="{{path_for('user-save-order', ['type' => 1])}}" title="">I Miei Ordini</a></li>
            <li>   <a href="{{ path_for('user-save-order', ['type' => 2]) }}" title="">Contenuti Scaricabili</a></li>
            <li>   <a href="{{path_for('area-user-impostazioni')}}" title="">Impostazioni Account</a></li>
            <li>   <a href="{{path_for('logout')}}?ref={{request()->path()}}" title="">Esci</a></li>


        </ul>
    </div><!-- /.widget widget-categories -->

</div><!-- /.sidebar left -->