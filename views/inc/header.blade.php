<div class="boxed">

    <div class="overlay"></div>

    <!-- Preloader -->
    {{--<div class="preloader">--}}
        {{--<div class="clear-loading loading-effect-2">--}}
            {{--<span></span>--}}
        {{--</div>--}}
    {{--</div><!-- /.preloader -->--}}
    <section id="header" class="header">
        <div class="header-top">
            <div class="container">

                <div class="row">
                    <div class="col-md-4">

                    </div><!-- /.col-md-4 -->
                    <div class="col-md-2">

                    </div><!-- /.col-md-4 -->
                    <div class="col-md-6">
                        <ul class="flat-unstyled">
                            @if(!user_logged())
                                <li class="account">
                                    <a href="{{path_for('loginp',array())}}" title=""><img width="20" style="margin-top:-2px;" src="{{asset('assets/images/')}}icons/utente-2.svg"> Accedi</a>
                                </li>
                                <li>
                                    <a href="{{ path_for('registrationp', array()) }}" title=""><img width="20" style="margin-top:-2px;" src="{{asset('assets/images/')}}icons/utente-add-2.svg"> Registrati</a>
                                </li>
                            @else
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink"
                                       data-toggle="dropdown"
                                       aria-haspopup="true" aria-expanded="false">
                                        Ciao {{user_logged()->name}}
                                    </a>
                                    <div class="dropdown-menu p-3" aria-labelledby="navbarDropdownMenuLink">
                                        <h5><a href="{{path_for('area-user')}}">Area Utente</a></h5>
                                        <p>
                                            <a href="{{path_for('logout')}}?ref={{request()->path()}}">Logout</a>
                                        </p>
                                    </div>
                                </li>
                            @endif
                            <!--
                            <li>
                                <a href="{{ path_for('checkout') }}" title=""><i class="fa fa-heart mr-1"></i> Lista dei
                                    desideri</a>
                            </li>-->
                            <li>
                                <a href="{{ path_for('checkout') }}" title=""><img width="20" style="margin-top:-2px;" src="{{asset('assets/images/')}}icons/carrello-2.svg"> <span
                                            class="badge badge-default"></span></a>
                            </li>
                        </ul><!-- /.flat-unstyled -->
                    </div><!-- /.col-md-4 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.header-top -->
        <div class="header-middle" style="margin-bottom:40px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="logo" class="logo style1 text-center d-none d-md-block">
                            <a href="{{ path_for('frontpage') }}" title="">
                                <img style="width:80%" src="{{asset('assets/images/')}}icons/logo-blob-video.png" alt="Blob">
                            </a>
                        </div><!-- /#logo -->
                        <div id="logo-mobile" class="d-block d-md-none">
                            <div class="row">
                                <div class="col-2 text-left">
                                    <a href="#" class="cart menuToggle" style="padding:0;"><img style="width:100%; margin:0;" src="{{asset('assets/images/')}}icons/menu.png"></a>
                                </div>
                                <div class="col-8 text-center">
                                    <a href="{{ path_for('frontpage') }}" title="">
                                        <img class="w-100" src="{{asset('assets/images/')}}icons/logo-blob-video.png" alt="Blob">
                                    </a>
                                </div>
                                <div class="col-2 text-right">
                                    <a href="{{ path_for('checkout') }}" class="cart"><img src="{{asset('assets/images/')}}icons/carrello-2.png"></a>
                                </div>
                            </div>
                        </div><!-- /#logo -->
                        <div class="nav-wrap">
                            <div id="mainnav" class="mainnav style1">
                                <ul class="menu">
                                    <li class="column-1">
                                        <a href="{{ path_for('frontpage') }}" title="">Home</a>
                                    </li>
                                    <li class="column-1">
                                        <a href="{{ path_for('single-page', ['permalink' => 'chi-siamo']) }}" title="">Chi
                                            Siamo</a>
                                    </li>
                                    <li class="column-1">
                                        <a href="#" title="">Specialisti dell'usato</a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="{{ path_for('single-page', ['permalink' => 'usato-gaming']) }}"
                                                   title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Usato
                                                    Gaming</a>
                                            </li>
                                            <li>
                                                <a href="{{ path_for('single-page', ['permalink' => 'usato-apple']) }}"
                                                   title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Usato
                                                    Apple</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="column-1">
                                        <a href="#" title="">Assistenza Specializzata</a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="{{ path_for('single-page', ['permalink' => 'assistenza-console']) }}"
                                                   title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Console
                                                    Gaming</a>
                                            </li>
                                            <li>
                                                <a href="{{ path_for('single-page', ['permalink' => 'assistenza-apple']) }}"
                                                   title=""><i class="fa fa-angle-right" aria-hidden="true"></i>iPhone, iPad e Mac</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="column-1">
                                        <a href="http://www.blobvideo.com/blog/" target="u_blank" title="">Blog</a>
                                    </li>
                                    <li class="column-1">
                                        <a href="{{ path_for('single-page', ['permalink' => 'contatti']) }}" title="">Contatti</a>
                                    </li>
                                </ul><!-- /.menu -->
                            </div><!-- /.mainnav -->
                        </div><!-- /.nav-wrap -->
                        <div class="btn-menu style1">
                            <span></span>
                        </div><!-- //mobile menu button -->
                        <ul class="flat-infomation style1">
                            <li class="phone">
                                <img src="{{asset('assets/images/')}}icons/telefono.svg" alt="" style="padding:0;margin:0;width:30px;">
                                <a href="tel:+390815152511" title="">081 5152511</a>
                            </li>
                        </ul><!-- /.flat-infomation -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.header-middle -->
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-2 d-none d-md-block">
                        <div id="mega-menu">
                            <div class="btn-mega"><span></span>Categorie Prodotti</div>
                            <ul class="menu">
                                @if($categorieRoot)
                                    @foreach($categorieRoot as $categoriaRoot)
                                        <li>
                                            <a href="{{ path_for('shop-page-cat', ['category' => $categoriaRoot->permalink]) }}"
                                               title="" class="dropdown">
												<span class="menu-img">
													<img src="{{config('httpmedia')."ecommerce/categorie/".$categoriaRoot->meta('imgicon')}}"
                                                         style="width: 30px; height:30px;">
												</span>
                                                <span class="menu-title">
													{{$categoriaRoot->title}}
												</span>
                                            </a>
                                            @if(count($categoriaRoot->categoryChild)>0)
                                                <div class="drop-menu" style="padding-right:235px;">
                                                    <?php $pos = 0;?>
                                                    @foreach($categoriaRoot->categoryChild as $catLivUno)
                                                        @if($pos%2==0)
                                                            <div class="one-third">
                                                                @endif
                                                                <div class="cat-title">
                                                                    <a href="{{ path_for('shop-page-cat', ['category' => $catLivUno->permalink]) }}">{{$catLivUno->title}}</a>
                                                                </div>
                                                                <ul>
                                                                    @if($catLivUno->categoryChild && count($catLivUno->categoryChild)>0)
                                                                        @foreach($catLivUno->categoryChild as $catLivDue)
                                                                            <li>
                                                                                <a href="{{ path_for('shop-page-cat', ['category' => $catLivDue->permalink]) }}">{{$catLivDue->title}}</a>
                                                                            </li>
                                                                        @endforeach
                                                                    @elseif($catLivUno->meta('imghighlight'))
                                                                        <li>
                                                                            <div class="banner-img">
                                                                                <a href="{{$catLivUno->meta('link_imghighlight')}}">
                                                                                    <img src="{{config('httpmedia')."ecommerce/categorie/".$catLivUno->meta('imghighlight')}}"
                                                                                         alt="">
                                                                                </a>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </li>
                                                                    @endif
                                                                </ul>

                                                                <?php $pos++;?>
                                                                @if($pos%2==0)
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                        <div class="banner-cat-menu">
                                                            <ul>
                                                                @for($i = 1; $i < 4; $i++)
                                                                    @if($categoriaRoot->meta('banner'.$i))
                                                                        <li>
                                                                            <div class="banner-img">
                                                                                <a href="{{($categoriaRoot->meta('link_banner'.$i)) ? $categoriaRoot->meta('link_banner'.$i) : '#'}}"><img
                                                                                            src="{{config('httpmedia')."ecommerce/categorie/".$categoriaRoot->meta('banner'.$i)}}"
                                                                                            alt=""></a>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </li>
                                                                    @endif
                                                                @endfor
                                                            </ul>
                                                        </div>
                                                    @if($pos%2!=0)
                                                </div>
                                            @endif
                                            @endif

                                        </li>
                                    @endforeach
                                @endif
                            </ul><!-- /.menu -->
                        </div>
                    </div><!-- /.col-md-3 col-2 -->
                    <div class="col-md-9 col-12">
                        <div class="top-search style1">
                            <form action="{{ path_for('shop-page-search') }}" method="get" class="form-search"
                                  accept-charset="utf-8">
                                <div class="cat-wrap cat-wrap-v1">
                                    <div class="selctfake">
                                        <div data-tipo value="">Tipologia</div>
                                    </div>
                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    <div class="all-categories">
                                        <div class="cat-list-search">
                                            <div class="title">
                                                Tipologie
                                            </div>
                                            <ul>
                                                <li data-idcateg="" data-title="Tutte">Tutte</li>
                                                @if($tipologie)
                                                    @foreach($tipologie as $tipologia)
                                                        <li data-idcateg="{{$tipologia->id}}" data-title="{{$tipologia->title}}">
                                                            {{$tipologia->title}}
                                                        </li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div><!-- /.cat-list-search -->
                                    </div><!-- /.all-categories -->
                                </div><!-- /.cat-wrap -->
                                <div class="box-search">
                                    <input type="hidden" name="typology" id="searchCat" value=""/>
                                    <input type="text" id="srcAjax" autocomplete="off" name="term" placeholder="Cerca prodotto">
                                    <span class="btn-search">
												<button type="submit" class="waves-effect"><img
                                                            src="{{asset('assets/images/')}}icons/search-2.png" alt=""></button>
											</span>
                                    <div class="search-suggestions">
                                        <div class="box-suggestions">
                                            <div class="title">
                                                Risultati
                                            </div>
                                            <ul class="result-ajax-search">
                                                <li class="loading"><i class="fa fa-spinner fa-spin"></i></li>
                                                <li class="nothing">Nessun risultato da mostrare...</li>
                                                {{--<li>--}}
                                                {{--<div class="image">--}}
                                                {{--<img src="{{asset('assets/images/')}}product/other/s05.jpg" alt="">--}}
                                                {{--</div>--}}
                                                {{--<div class="info-product">--}}
                                                {{--<div class="name">--}}
                                                {{--<a href="#" title="">Razer RZ02-01071500-R3M1</a>--}}
                                                {{--</div>--}}
                                                {{--<div class="price">--}}
                                                {{--<span class="sale">--}}
                                                {{--$50.00--}}
                                                {{--</span>--}}
                                                {{--<span class="regular">--}}
                                                {{--$2,999.00--}}
                                                {{--</span>--}}
                                                {{--</div>--}}
                                                {{--</div>--}}
                                                {{--</li>--}}

                                            </ul>
                                        </div><!-- /.box-suggestions -->
                                        {{--<div class="box-cat">--}}
                                            {{--<div class="cat-list-search">--}}
                                                {{--<div class="title">--}}
                                                    {{--Tipologie--}}
                                                {{--</div>--}}
                                                {{--<ul class="result-ajax-search-cat">--}}
                                                    {{--<li class="loading"><i class="fa fa-spinner fa-spin"></i></li>--}}
                                                    {{--<!--<li class="nothing">Nessun risultato da mostrare...</li>-->--}}
                                                    {{--@if($tipologie)--}}
                                                        {{--@foreach($tipologie as $tipologia)--}}
                                                            {{--<li data-idcateg="{{$tipologia->id}}">--}}
                                                                {{--{{$tipologia->title}}--}}
                                                            {{--</li>--}}
                                                        {{--@endforeach--}}
                                                    {{--@endif--}}
                                                {{--</ul>--}}
                                            {{--</div><!-- /.cat-list-search -->--}}
                                        {{--</div><!-- /.box-cat -->--}}
                                    </div><!-- /.search-suggestions -->
                                </div><!-- /.box-search -->
                            </form><!-- /.form-search -->
                        </div><!-- /.top-search -->
                        <span class="show-search">
									<button></button>
								</span><!-- /.show-search -->
                        <div class="text-right d-none d-md-block">

                            <a href="/offerte" class="d-inline-block font-weight-bold mr-3 mr-md-0"
                               style="font-size:1.3em;color:#ffffff; background: #DD1C1A;padding:25px 30px 10px 30px;margin-top:-4px;">{{--<img src="{{asset('assets/images/')}}icons/nataleicon.png" style="display: inline-block; margin-right: 5px; width: 28px; position: relative; top:-4px;">--}} Offerte</a>
                        </div>
                    </div><!-- /.col-md-9 col-10 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.header-bottom -->
    </section><!-- /#header -->