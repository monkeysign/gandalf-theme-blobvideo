$(document).ready(function() {
//    $(".select2").select2();
    
    $('#form-save').validator().on('submit', function(e) {
        if (e.isDefaultPrevented()) {
            // Errori di validazione
            return false;
        } else {

            //block ui
            $('#panelblk').block({
                message: '<h3>Attendi</h3>'
                , overlayCSS: {
                    backgroundColor: '#eaeaea'
                }
                , css: {
                    border: '1px solid #fff'
                }
            });

            // Submit form
            var $form = $('#form-save')
            var formData = new FormData($form[0]);

            $.ajax({
                method: "POST",
                url: $form.attr("action"),
                data: formData,
                success: function(data) {
                    if (data.state === true) {
                        //setto id nascosto
                        if(parseInt($('#item_id_hidden').val()) <= 0){
                            location.href = './' + data.record.id;
                        }
                        $('#item_id_hidden').val(data.record.id);
                        //sblocco table
                        $('#panelblk').unblock();
                        //messaggio
                        $.toast({
                            heading: 'Tutto ok!',
                            text: data.mex,
                            position: 'bottom-right',
                            loaderBg: '#ff6849',
                            icon: 'success',
                            hideAfter: 2000

                        });
                    } else {

                        $('#panelblk').unblock();
                        //messaggio
                        $.toast({
                            heading: 'Errore',
                            text: data.mex,
                            position: 'bottom-right',
                            loaderBg: '#fb9678',
                            icon: 'error',
                            hideAfter: 8000

                        });
                    }
                    return false;
                },
                cache: false,
                contentType: false,
                processData: false
            })

            return false;
        }
    });
});